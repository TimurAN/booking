from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver
from company.models import Company


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    name = models.CharField(max_length=200, verbose_name='ФИО', null=True, )
    email = models.EmailField(null=True, )
    is_employer = models.BooleanField(default=False)
    is_client = models.BooleanField(default=False)
    is_company = models.BooleanField(default=False)
    is_remotely = models.BooleanField(default=False)
    phone = models.CharField(max_length=30, verbose_name='Телефон', null=True, blank=True)
    # address = models.CharField(max_length=1000, verbose_name='Место')
    visibility = models.BooleanField(default=True, verbose_name='Видимость')
    image = models.ImageField(upload_to='user', default='user/profile_photo.png',
                              verbose_name='Фотография', null=True, blank=True)
    country = models.CharField(max_length=200, verbose_name='Страна', null=True, blank=True)
    city = models.CharField(max_length=200, verbose_name='Город', null=True, blank=True)
    is_active = models.BooleanField(default=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, null=True, blank=True, related_name='employer')
    information = models.TextField(null=True, blank=True)
    email_confirmed = models.BooleanField(default=False)
    profession = models.CharField(max_length=200, verbose_name='Профессия', null=True, )

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
