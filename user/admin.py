from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext, gettext_lazy as _

from .models import *


# Register your models here.


class ProfileInline(admin.StackedInline):
    model = Profile


class MyUserAdmin(UserAdmin):
    inlines = [ProfileInline]
    list_display = ['id', 'username', 'profile', 'is_active']
    list_display_links = list_display
    ordering = ['-id']

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        # (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {
            # 'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
            # 'fields': ('is_active', 'is_staff', 'is_superuser'),
            'fields': ('is_active',),
        }),
        # (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )


admin.site.unregister(User)
admin.site.register(User, MyUserAdmin)
