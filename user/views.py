from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.views.generic import TemplateView
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required
from category.models import Category
from user.forms import UserUpdateForm, ProfileUpdateForm, SignUpForm
from django.utils.decorators import method_decorator

from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string
from django.utils.encoding import force_text

from user.tokens import account_activation_token
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.models import User


def account_activation_sent(request):
    return render(request, 'user/account_activation_sent.html')


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.email = user.username
            user.is_active = False
            user.save()
            user.profile.is_client = True
            user.save()
            current_site = get_current_site(request)
            subject = 'Activate Your MySite Account'
            message = render_to_string('user/account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message)
            return redirect('account_activation_sent')
    else:
        form = SignUpForm()
    return render(request, 'user/signup.html', {'form': form})


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()
        login(request, user)
        return redirect('index')
    else:
        return render(request, 'user/account_activation_invalid.html')


@method_decorator(login_required, name='dispatch')
class CustomerAreaView(TemplateView):
    template_name = 'user/customer_profile.html'

    def get(self, request, *args, **kwargs):
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)
        return render(request, self.template_name, context={
            'u_form': u_form,
            'p_form': p_form,
        })

    def post(self, request, *args, **kwargs):
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)

        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            return redirect('profile')
        return render(request, self.template_name, context={
            'u_form': u_form,
            'p_form': p_form,
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CustomerAreaView, self).dispatch(*args, **kwargs)


@method_decorator(login_required, name='dispatch')
class CustomerBookingsView(TemplateView):
    template_name = 'user/bookings.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context={

        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CustomerBookingsView, self).dispatch(*args, **kwargs)


@method_decorator(login_required, name='dispatch')
class LogoutRequest(TemplateView):

    def get(self, request, *args, **kwargs):
        logout(request)
        messages.info(request, 'Logged out')
        return redirect('login')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LogoutRequest, self).dispatch(*args, **kwargs)


def logout_request(request):
    logout(request)
    messages.info(request, 'Logged out')
    return redirect('login')


class LoginRequest(TemplateView):
    template_name = 'main/index.html'

    def get(self, request, *args, **kwargs):
        form = AuthenticationForm()
        categories = Category.objects.all()
        return render(request, self.template_name, context={
            'form': form,
            'categories': categories,
        })

    def post(self, request, *args, **kwargs):
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('check_profile')
        return render(request, self.template_name, context={
            'form': form,
        })


@method_decorator(login_required, name='dispatch')
class CheckProfile(TemplateView):
    def get(self, request, *args, **kwargs):
        user = request.user
        try:
            if user.profile.is_company:
                if user.profile.company:
                    return redirect('company')
                else:
                    return redirect('create_company')
            elif user.profile.is_employer:
                return redirect('employer_bookings')
            else:
                return redirect('customer_settings')

        except(Exception, NameError):
            return redirect('customer_settings')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CheckProfile, self).dispatch(*args, **kwargs)
