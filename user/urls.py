"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from user import views, tokens
from user import views as core_views
from user.views import *
from django.conf.urls import url

urlpatterns = [
    path('customer-profile/', CustomerAreaView.as_view(), name='profile'),
    path('bookings/', CustomerBookingsView.as_view(), name='my-bookings'),
    path('', LoginRequest.as_view(), name='login'),
    path('logout/', LogoutRequest.as_view(), name='logout'),
    path('check/profile', CheckProfile.as_view(), name='check_profile'),
    path('sing_up/', core_views.signup, name='signup'),
    url(r'^account_activation_sent/$', core_views.account_activation_sent, name='account_activation_sent'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        core_views.activate, name='activate'),
]
