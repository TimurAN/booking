from django.contrib import admin
from .models import *


# Register your models here.


class EmployerDayModeAdmin(admin.ModelAdmin):
    list_display = ["employer", "day"]
    search_fields = ["employer"]


admin.site.register(EmployerDayMode, EmployerDayModeAdmin)
