from company.models import Service
from employer.models import EmployerDayMode


def update_service(user, form):
    Service.objects.filter(employer_id=user.id).delete()

    for key in form:
        if key.__contains__('sub_category'):
            sub_category = form[key]
            price = form[sub_category]
            Service.objects.create(service_sub_category_id=sub_category, employer=user, price=price).save()


def update_graphic(user, form):
    EmployerDayMode.objects.filter(employer=user).delete()

    for key in form:
        if key.__contains__('check_'):
            day = key[-1]
            key = key.replace('check_', '')
            key = key.replace(day, '')
            start_work = form[f'{key}start_work']
            end_work = form[f'{key}end_work']
            start_break = form[f'{key}start_break']
            end_break = form[f'{key}end_break']
            is_break = False
            if f'{key}break' in form:
                is_break = True

            EmployerDayMode.objects.create(
                employer=user,
                working_day_is_active=True,
                day=day,
                start_work=start_work,
                end_work=end_work,
                is_break=is_break,
                start_break=start_break,
                end_break=end_break,
            )


def prepare_days(day_modes):
    prep_days = []
    for i in range(7):
        for day_mode in day_modes:
            if i == day_mode.day:
                prep_days.append(day_mode)
                break
        else:
            empty_day = {'day': None}
            prep_days.append(empty_day)
    return prep_days
