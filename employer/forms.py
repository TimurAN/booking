from django import forms
from django.forms.widgets import TextInput

from user.models import *


class PhoneInput(TextInput):
    input_type = 'tel'


class EmployerForm(forms.ModelForm):
    ACTIVE = (
        (True, 'Активный'),
        (False, 'Неактивный'),
    )

    visibility = forms.ChoiceField(choices=ACTIVE)

    class Meta:
        model = Profile
        fields = ['name', 'email', 'phone', 'visibility', 'information', 'image']
        widgets = {
            # 'information': forms.Textarea(attrs={'width': 10, 'rows': 70}),
            'phone': PhoneInput(),
        }
