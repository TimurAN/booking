from customer.models import Booking, Period


def get_all_time(day, b_date):
    start = [day.start_work.hour, day.start_work.minute]
    end = [day.end_work.hour, day.end_work.minute]
    full_day = []
    while True:

        if end[0] <= start[0] and end[1] <= start[1]:
            break
        old_start = start[:]
        start[1] += 30

        if start[1] > 59:
            start[1] -= 60
            start[0] += 1
        full_day.append([old_start[0], old_start[1], start[0], start[1], 0])

    p = [day.start_break, day.end_break]
    if day.is_break:
        set_work(p, full_day)


    set_booking_time(day, b_date, full_day)

    # for i in full_day:
    #     print(i)
    return full_day


def set_work(period, full_day):
    # print('*** ', period[0].hour, period[0].minute)
    flag = False
    for i in full_day:
        if i[0] == period[0].hour and i[1] == period[0].minute:
            flag = True
            i[4] = 1

        if i[0] == period[1].hour and i[1] == period[1].minute:
            break

        if flag:
            i[4] = 1

    # if employer has another bookings this day

    return full_day

def set_booking_time(day, b_date, full_day):
    bookings = Booking.objects.filter(employer=day.employer, booking_date=b_date)


    if bookings:
        for b in bookings:
            periods = b.booking_periods.all()
            # priint(periods)
            if periods:
                for b in periods:
                    p = [b.start_period, b.end_period]
                    # print("r = {}".format(p))
                    set_work(p, full_day)


def out_period(full_day):
    period = []
    for i in full_day:
        item = ''
        if i[0] == 0:
            item += '00'
        else:
            item += str(i[0])

        item += ':'
        if i[1] == 0:
            item += '00'
        else:
            item += str(i[1])
        item += ' - '

        if i[2] == 0:
            item += '00'
        else:
            item += str(i[2])
        item += ':'

        if i[3] == 0:
            item += '00'
        else:
            item += str(i[3])

        period.append({'time': item, 'status': i[4]})
    return period
