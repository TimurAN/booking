from django.db import models
from django.contrib.auth.models import User
from datetime import date


class EmployerDayMode(models.Model):
    employer = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, default=None,
                                 related_name='employer_day_mode')

    WEEK = ((0, "Понедельник"), (1, "Вторник"), (2, "Среда"), (3, "Четверг"), (4, "Пятница"), (5, "Суббота"),
            (6, "Воскресенье"))

    working_day_is_active = models.BooleanField(default=False)

    day = models.IntegerField(default=0, choices=WEEK)

    start_work = models.TimeField()
    end_work = models.TimeField()

    # if we have break
    is_break = models.BooleanField()
    start_break = models.TimeField()
    end_break = models.TimeField()

    class Meta:
        unique_together = ('employer', 'day')

    def __str__(self):
        return self.WEEK[self.day][1]


class EmployerHoliday(models.Model):
    date = models.DateField()
    employer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='employer_holiday')

    def __str__(self):
        return self.date
