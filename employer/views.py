import json
from django.core import serializers
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

from booking.decorators import employer_required
from .forms import EmployerForm
from .models import *
from datetime import date
from .diary import get_all_time
from customer.models import Booking, Period
from company.models import *
from .utils import update_service, update_graphic, prepare_days
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _


@method_decorator((login_required, employer_required), name='dispatch')
class EmployerCustomersView(TemplateView):
    template_name = 'employer/booking_list.html'

    def get(self, request, *args, **kwargs):
        bookings = Booking.objects.filter(employer=request.user)
        return render(request, self.template_name, context={
            'bookings': bookings
        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EmployerCustomersView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, employer_required), name='dispatch')
class EmployerSettingsView(TemplateView):
    template_name = 'employer/update_employer.html'

    def get(self, request, *args, **kwargs):
        form = EmployerForm(instance=request.user.profile)
        category = Category.objects.get(company__employer__user=request.user)
        services = Service.objects.select_related('service_sub_category').filter(employer_id=request.user.id)
        day_modes = EmployerDayMode.objects.filter(employer=request.user)
        sub_category_addition = []
        for service in services:
            sub_category_addition.append(service.service_sub_category)
        day_modes = prepare_days(day_modes)

        return render(request, self.template_name, context={
            'form': form,
            'category': category,
            'services': services,
            'sub_category_addition': sub_category_addition,
            'day_modes': day_modes,
        })

    def post(self, request, *args, **kwargs):
        form = EmployerForm(request.POST or None, request.FILES or None, instance=request.user.profile)
        if form.is_valid():
            form.save()
            update_service(request.user, form.data)

            update_graphic(request.user, form.data)
            messages.info(request, _('Successfully updated'))
        return redirect('employer_settings')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EmployerSettingsView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, employer_required), name='dispatch')
class EmployerPhotosView(TemplateView):
    template_name = 'employer/company_photo.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context={

        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EmployerPhotosView, self).dispatch(*args, **kwargs)
