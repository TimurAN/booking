from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.db.models import Q
# Create your views here.
from category.models import *


class MainView(TemplateView):
    template_name = 'main/index.html'

    def get(self, request, *args, **kwargs):
        form = AuthenticationForm()
        categories = Category.objects.all()
        return render(request, self.template_name, context={
            'form': form,
            'categories': categories,
        })

    def post(self, request, *args, **kwargs):
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('company')
        return render(request, self.template_name, context={
            'form': form,
        })
