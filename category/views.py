from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from category.forms import CategoryForm
from django.utils.decorators import method_decorator


@method_decorator(login_required, name='dispatch')
class CategoryView(TemplateView):
    template_name = 'category/category.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context={

        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CategoryView, self).dispatch(*args, **kwargs)


def category(request):
    template_name = 'category/category.html'

    form = CategoryForm()
    return render(request, template_name, context={
        'form': form
    })
    # return HttpResponse('test')
