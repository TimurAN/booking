from django.contrib import admin
from modeltranslation.admin import TranslationAdmin
from category.models import Category
from company.models import ServiceSubCategory


class ServiceSubCategoryInline(admin.TabularInline):
    model = ServiceSubCategory


class CategoryAdmin(admin.ModelAdmin):
    list_display = ["title", "image"]
    search_fields = ["title"]
    inlines = [ServiceSubCategoryInline, ]


class CategoryTranslatedEntryAdmin(CategoryAdmin, TranslationAdmin):
    pass


admin.site.register(Category, CategoryTranslatedEntryAdmin)
