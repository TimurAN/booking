from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=64)
    image = models.ImageField(upload_to='image_of_categories', default='image_of_categories/default_logo.png')

    def __str__(self):
        return self.title
