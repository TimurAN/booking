from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils.translation import ugettext_lazy as _

from user.models import User


def company_required(function):
    def wrap(request, *args, **kwargs):
        user = User.objects.get(id=request.user.id)
        if user.profile.is_company:
            return function(request, *args, **kwargs)
        else:
            messages.info(request, _('Access denied for Company, You have no right !!!'))
            return redirect('index')
    return wrap


def employer_required(function):
    def wrap(request, *args, **kwargs):
        user = User.objects.get(id=request.user.id)
        if user.profile.is_employer:
            return function(request, *args, **kwargs)
        else:
            messages.info(request, _('Access denied for Employee, You have no right !!!'))
            return redirect('index')
    return wrap


def client_required(function):
    def wrap(request, *args, **kwargs):
        user = User.objects.get(id=request.user.id)
        if user.profile.is_client:
            return function(request, *args, **kwargs)
        else:
            messages.info(request, _('Access denied for Client, You have no right !!!'))
            return redirect('index')
    return wrap
