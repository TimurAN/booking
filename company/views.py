from django.contrib import messages
from django.contrib.auth.hashers import make_password
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils.timezone import now
from django.views.generic import TemplateView

import json
from django.core import serializers
from django.utils.safestring import SafeString

from booking.decorators import company_required
from company.forms import *
from employer.forms import EmployerForm
from employer.utils import prepare_days, update_service, update_graphic
from .models import *
from employer.models import *
from customer.models import *
from .utils import create_service, create_graphic, create_not_working_days
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from datetime import datetime
from django.utils.translation import ugettext_lazy as _


@method_decorator((login_required, company_required), name='dispatch')
class CompanyCalendarView(TemplateView):
    template_name = 'company/calendar.html'

    def get(self, request, *args, **kwargs):
        array_bookings = []
        bookings = Booking.objects.filter(company__user=request.user)

        for book in bookings:
            periods = Period.objects.filter(booking=book.pk)
            json_persiods = serializers.serialize('json', periods)
            json_persiods = json.loads(json_persiods)
            for period in json_persiods:
                array_bookings.append({
                    'date': datetime.strptime(str(book.booking_date), '%Y-%m-%d').strftime('%Y-%m-%d'),
                    'status': book.status,
                    'name': book.contact_name,
                    'email': book.contact_email,
                    'phone_number': book.contact_phone,
                    'start_time': period['fields']['start_period'],
                    'end_time': period['fields']['end_period']
                })

        company = request.user.profile.company
        employers = User.objects.filter(profile__company=company, profile__is_company=False)

        return render(request, self.template_name, context={
            'bookings': SafeString(array_bookings),
            'employers': employers
        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyCalendarView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, company_required), name='dispatch')
class CompanyCalendarByEmployerView(TemplateView):
    template_name = 'company/calendar.html'

    def get(self, request, *args, **kwargs):
        id = kwargs['emp_id']
        array_bookings = []
        bookings = Booking.objects.filter(employer_id=id)

        for book in bookings:
            periods = Period.objects.filter(booking=book.pk)
            json_persiods = serializers.serialize('json', periods)
            json_persiods = json.loads(json_persiods)
            for period in json_persiods:
                array_bookings.append({
                    'date': datetime.strptime(str(book.booking_date), '%Y-%m-%d').strftime('%Y-%m-%d'),
                    'status': book.status,
                    'name': book.contact_name,
                    'email': book.contact_email,
                    'phone_number': book.contact_phone,
                    'start_time': period['fields']['start_period'],
                    'end_time': period['fields']['end_period']
                })

        company = request.user.profile.company
        employers = User.objects.filter(profile__company=company, profile__is_company=False)

        return render(request, self.template_name, context={
            'bookings': SafeString(array_bookings),
            'employers': employers
        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyCalendarByEmployerView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, company_required), name='dispatch')
class CompanyEmployerView(TemplateView):
    template_name = 'company/employer_list.html'

    def get(self, request, *args, **kwargs):
        company = request.user.profile.company
        employers = User.objects.select_related('profile').filter(profile__company=company, profile__is_company=False)
        return render(request, self.template_name, context={
            'employers': employers
        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyEmployerView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, company_required), name='dispatch')
class CompanyMeetView(TemplateView):
    template_name = 'company/meet.html'

    def get(self, request, *args, **kwargs):
        bookings = Booking.objects.filter(company__user=request.user)
        information = Booking.objects.filter(company__user=request.user)

        employers = User.objects.filter(profile__company=request.user.profile.company, profile__is_employer=True)

        customers = Booking.objects.filter(company=request.user.profile.company)

        customers = customers.values_list('contact_name', flat=True).distinct()

        services = Category.objects.get(title=request.user.profile.company.category)
        statuses = Booking.WEEK
        number = request.GET.get('number')
        by_date = request.GET.get('by_date')
        employer = request.GET.get('employer')
        customer = request.GET.get('customer')
        service = request.GET.get('service')
        status = request.GET.get('status')
        if number:
            bookings = bookings.filter(id=number)
        if by_date:
            datetime_object = datetime.strptime(str(by_date), '%d.%m.%Y').strftime('%Y-%m-%d')
            bookings = bookings.filter(create_date__contains=str(datetime_object))
        if employer:
            bookings = bookings.filter(employer__profile__name__icontains=employer)
        if customer:
            bookings = bookings.filter(contact_name__icontains=customer)
        if service:
            bookings = bookings.filter(service__service_sub_category__name__icontains=service)
        if status:
            bookings = bookings.filter(status__icontains=status)
        return render(request, self.template_name, context={
            'bookings': bookings,
            'information': information,
            'employers': employers,
            'customers': customers,
            'statuses': statuses,
            'services': services.sub_category.all(),
        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyMeetView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, company_required), name='dispatch')
class CompanyCreateEmployerView(TemplateView):
    template_name = 'company/create_employer.html'

    def get(self, request, *args, **kwargs):
        form = EmployerForm()
        # service_category = Category.objects.all()
        category = Category.objects.get(company__user=request.user)
        # print(service_category)
        return render(request, self.template_name, context={
            'form': form,
            'category': category,
        })

    def post(self, request, *args, **kwargs):
        form = EmployerForm(request.POST or None, request.FILES or None)

        if form.is_valid():
            phone = request.POST['phone']
            user = User.objects.create_user(username=phone, password=phone)
            profile = Profile.objects.get(user=user)
            instance = form.save(commit=False)
            profile.name = instance.name
            profile.phone = instance.phone
            profile.email = instance.email
            profile.visibility = instance.visibility
            profile.information = instance.information
            profile.image = instance.image
            profile.is_employer = True
            profile.company = request.user.company
            create_service(user, form.data, )
            profile.save()
            create_graphic(user, form.data)
            create_not_working_days(user, form.data)
            return redirect('company_employer_list')
        return render(request, self.template_name, context={
            'form': form,
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyCreateEmployerView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, company_required), name='dispatch')
class CompanyEmployerSettingsView(TemplateView):
    template_name = 'company/update_employer.html'

    def get(self, request, *args, **kwargs):
        employer_id = kwargs['employer_id']
        employer = User.objects.get(id=employer_id)
        form = EmployerForm(instance=employer.profile)
        category = Category.objects.get(company__employer__user=employer)
        services = Service.objects.select_related('service_sub_category').filter(employer_id=employer.id)
        day_modes = EmployerDayMode.objects.filter(employer=employer)
        ######################################################################
        hodidays = EmployerHoliday.objects.filter(employer=employer)
        h_list = []
        for i in hodidays:
            month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
                     'November', 'December']

            h_list.append((str(i.date.year), month[i.date.month - 1], str(i.date.day), i.date.month))
        print(h_list)
        ######################################################################

        sub_category_addition = []
        for service in services:
            sub_category_addition.append(service.service_sub_category)
        day_modes = prepare_days(day_modes)
        return render(request, self.template_name, context={
            'form': form,
            'category': category,
            'services': services,
            'sub_category_addition': sub_category_addition,
            'day_modes': day_modes,
            'employer': employer,
            'holidays': h_list
        })

    def post(self, request, *args, **kwargs):
        employer_id = kwargs['employer_id']
        employer = User.objects.get(id=employer_id)
        form = EmployerForm(request.POST or None, request.FILES or None, instance=employer.profile)
        if form.is_valid():
            form.save()
            update_service(employer, form.data)
            update_graphic(employer, form.data)
            messages.info(request, _('Successfully updated'))
            create_not_working_days(employer, form.data)
        return redirect('company_update_employer', employer_id)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyEmployerSettingsView, self).dispatch(*args, **kwargs)


# @method_decorator(login_required, name='dispatch')
# class CompanyServiceEmployerView(TemplateView):
#     template_name = 'company/html-6-old.html'
#
#     def get(self, request, *args, **kwargs):
#         return render(request, self.template_name, context={
#
#         })
#
#     def post(self, request, *args, **kwargs):
#         return render(request, self.template_name, context={
#         })
#
#     @method_decorator(login_required)
#     def dispatch(self, *args, **kwargs):
#         return super(CompanyServiceEmployerView, self).dispatch(*args, **kwargs)
#
#
# @method_decorator(login_required, name='dispatch')
# class CompanyGraphicEmployerView(TemplateView):
#     template_name = 'company/graphic_employer.html'
#
#     def get(self, request, *args, **kwargs):
#         return render(request, self.template_name, context={
#
#         })
#
#     def post(self, request, *args, **kwargs):
#         return render(request, self.template_name, context={
#         })
#
#     @method_decorator(login_required)
#     def dispatch(self, *args, **kwargs):
#         return super(CompanyGraphicEmployerView, self).dispatch(*args, **kwargs)
#
#
# @method_decorator(login_required, name='dispatch')
# class CompanyWeekendEmployerView(TemplateView):
#     template_name = 'company/html-8.html'
#
#     def get(self, request, *args, **kwargs):
#         return render(request, self.template_name, context={
#
#         })
#
#     def post(self, request, *args, **kwargs):
#         return render(request, self.template_name, context={
#         })
#
#     @method_decorator(login_required)
#     def dispatch(self, *args, **kwargs):
#         return super(CompanyWeekendEmployerView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, company_required), name='dispatch')
class CompanyCustomersView(TemplateView):
    template_name = 'company/booking_list.html'

    def get(self, request, *args, **kwargs):
        search = request.GET.get('search_by_customer')
        bookings = Booking.objects.filter(company__user=request.user)

        if search:
            bookings = bookings.filter(contact_name__icontains=search)
        return render(request, self.template_name, context={
            'bookings': bookings,
        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyCustomersView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, company_required), name='dispatch')
class CompanyMainView(TemplateView):
    template_name = 'company/company_settings.html'

    def get(self, request, *args, **kwargs):
        try:
            company = Company.objects.get(user=request.user)
            form = CompanyForm(instance=company)
        except:
            form = CompanyForm()
        return render(request, self.template_name, context={
            'form': form,
        })

    def post(self, request, *args, **kwargs):

        try:
            company = Company.objects.get(user=request.user)
            form = CompanyForm(request.POST, request.FILES, instance=company)
        except:
            form = CompanyForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()
            user = request.user
            user.profile.company = instance
            user.profile.save()
            return redirect('company-information-contacts')

        return render(request, self.template_name, context={
            'form': form,
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyMainView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, company_required), name='dispatch')
class CompanyInformationContactView(TemplateView):
    template_name = 'company/company_contacts.html'

    def get(self, request, *args, **kwargs):
        try:
            company = Company.objects.get(user=request.user)
            contact = Contact.objects.get(company=company)
            form = ContactForm(instance=contact)
        except:
            form = ContactForm()
        return render(request, self.template_name, context={
            'form': form,
        })

    def post(self, request, *args, **kwargs):

        try:
            company = Company.objects.get(user=request.user)
            contact = Contact.objects.get(company=company)
            form = ContactForm(request.POST, request.FILES, instance=contact)
        except:
            form = ContactForm(request.POST, request.FILES)

        if form.is_valid():
            instance = form.save(commit=False)
            instance.company = Company.objects.get(user=request.user)
            instance.save()
            return redirect('company-information-description')

        return render(request, self.template_name, context={
            'form': form,
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyInformationContactView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, company_required), name='dispatch')
class CompanyInformationDescriptionView(TemplateView):
    template_name = 'company/company_description.html'

    def get(self, request, *args, **kwargs):
        try:
            company = Company.objects.get(user=request.user)
            description = Description.objects.get(company=company)
            form = DescriptionForm(instance=description)
        except:
            form = DescriptionForm()
        return render(request, self.template_name, context={
            'form': form,
        })

    def post(self, request, *args, **kwargs):
        try:
            company = Company.objects.get(user=request.user)
            description = Description.objects.get(company=company)
            form = DescriptionForm(request.POST, request.FILES, instance=description)
        except:
            form = DescriptionForm(request.POST, request.FILES)

        if form.is_valid():
            instance = form.save(commit=False)
            instance.company = Company.objects.get(user=request.user)
            instance.save()
            return redirect('company-information-photo')

        return render(request, self.template_name, context={
            'form': form,
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyInformationDescriptionView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, company_required), name='dispatch')
class CompanyInformationPhotoView(TemplateView):
    template_name = 'company/company_photo.html'

    def get(self, request, *args, **kwargs):
        form = ImageForm()
        return render(request, self.template_name, context={
            'form': form,
        })

    def post(self, request, *args, **kwargs):
        form = ImageForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.company = Company.objects.get(user=request.user)
            instance.save()
            return redirect('company-information-requisite')

        return render(request, self.template_name, context={
            'form': form,
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyInformationPhotoView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, company_required), name='dispatch')
class CompanyInformationRequisiteView(TemplateView):
    template_name = 'company/company_requisite.html'

    def get(self, request, *args, **kwargs):
        try:
            company = Company.objects.get(user=request.user)
            requisite = Requisite.objects.get(company=company)
            form = RequisiteForm(instance=requisite)
        except:
            form = RequisiteForm()
        return render(request, self.template_name, context={
            'form': form,
        })

    def post(self, request, *args, **kwargs):
        try:
            company = Company.objects.get(user=request.user)
            requisite = Requisite.objects.get(company=company)
            form = RequisiteForm(request.POST, request.FILES, instance=requisite)
        except:
            form = RequisiteForm(request.POST, request.FILES)

        if form.is_valid():
            instance = form.save(commit=False)
            instance.company = Company.objects.get(user=request.user)
            instance.save()
            return redirect('create_company')

        return render(request, self.template_name, context={
            'form': form,
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyInformationRequisiteView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, company_required), name='dispatch')
class CompanyServicesView(TemplateView):
    template_name = 'company/html-14.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context={

        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyServicesView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, company_required), name='dispatch')
class CompanyScheduleView(TemplateView):
    template_name = 'company/company_graphic.html'

    def get(self, request, *args, **kwargs):
        service_sub_category = request.GET.get('service_sub_category')
        users = User.objects.filter(profile__company__user=request.user, profile__is_employer=True)
        sub_categories = ServiceSubCategory.objects.filter(
            category__title=request.user.company.category.title)

        if service_sub_category:
            users = users.filter(services__service_sub_category__name=service_sub_category)
        return render(request, self.template_name, context={
            'employers': users,
            'sub_categories': sub_categories,
        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyScheduleView, self).dispatch(*args, **kwargs)


@method_decorator((login_required, company_required), name='dispatch')
class CompanyUsersView(TemplateView):
    template_name = 'company/company_users.html'

    def get(self, request, *args, **kwargs):
        employers = User.objects.filter(profile__is_employer=True)
        user_company = request.user.profile.company
        employers = employers.filter(profile__company=user_company)

        return render(request, self.template_name, context={
            'employers': employers,
        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyUsersView, self).dispatch(*args, **kwargs)


@method_decorator(login_required, name='dispatch')
class DeleteEmployerImageView(TemplateView):
    def get(self, request, *args, **kwargs):
        employer_id = kwargs['employer_id']
        User.objects.get(id=employer_id).profile.image.delete(save=True)
        messages.info(request, 'Successfully deleted')
        if request.user.profile.is_company:
            return redirect('company_update_employer', employer_id)
        else:
            return redirect('employer_settings')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DeleteEmployerImageView, self).dispatch(*args, **kwargs)
