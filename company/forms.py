from django import forms
from django.contrib.auth.models import User

from category.models import Category
from company.models import Company, Contact, Description, Image, Requisite


class CompanyForm(forms.ModelForm):
    category = forms.ModelChoiceField(queryset=Category.objects.all())

    class Meta:
        model = Company
        fields = '__all__'


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = '__all__'


class DescriptionForm(forms.ModelForm):
    class Meta:
        model = Description
        fields = '__all__'


class ImageForm(forms.ModelForm):
    image = forms.ImageField(required=False)

    class Meta:
        model = Image
        fields = '__all__'


class RequisiteForm(forms.ModelForm):
    class Meta:
        model = Requisite
        fields = '__all__'
