from django.contrib.auth.models import User
from django.db import models

from category.models import Category
from django.utils import timezone


class Company(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True, default=None)
    company_name = models.CharField(max_length=128, blank=True, null=True)
    category = models.ForeignKey(Category, models.CASCADE, blank=True, null=True, default=None)
    current_time = models.DateTimeField(default=timezone.now, blank=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    logo = models.ImageField(upload_to='logo_of_companies', default='logo_of_companies/default_logo.png')
    is_active = models.BooleanField(default=False)

    opened_time = models.TimeField(default='08:00:00', null=True, blank=True)
    closed_time = models.TimeField(default='18:00:00', null=True, blank=True)

    def __str__(self):
        return self.company_name


class Contact(models.Model):
    company = models.OneToOneField(Company, on_delete=models.CASCADE, blank=True, null=True, default=None)
    employer = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True, default=None)
    address = models.CharField(max_length=256, blank=True, null=True)
    index = models.CharField(max_length=64, blank=True, null=True)
    region = models.CharField(max_length=64, blank=True, null=True)
    phone_number = models.CharField(max_length=64, blank=True, null=True)
    schedule = models.CharField(max_length=64, blank=True, null=True)
    latitude = models.CharField(max_length=64, blank=True, null=True)
    longitude = models.CharField(max_length=64, blank=True, null=True)
    # def __str__(self):
    #     return self.


class Description(models.Model):
    company = models.OneToOneField(Company, on_delete=models.CASCADE, blank=True, null=True, default=None)
    employer = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True, default=None)
    description = models.TextField(default=None, blank=True)

    def __str__(self):
        return self.company.company_name


class Image(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, blank=True, null=True, default=None)
    employer = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, default=None)
    image = models.ImageField(upload_to='image_of_companies', default='image_of_companies/default_logo.png')

    def __str__(self):
        return self.company.company_name


class Requisite(models.Model):
    company = models.OneToOneField(Company, on_delete=models.CASCADE, blank=True, null=True, default=None)
    employer = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True, default=None)
    type = models.CharField(max_length=128, blank=True, null=True)
    legal_entity = models.CharField(max_length=128, blank=True, null=True)
    inn = models.CharField(max_length=128, blank=True, null=True)
    kpp = models.CharField(max_length=128, blank=True, null=True)
    bik = models.CharField(max_length=128, blank=True, null=True)
    legal_address = models.CharField(max_length=128, blank=True, null=True)
    actual_address = models.CharField(max_length=128, blank=True, null=True)
    bank = models.CharField(max_length=128, blank=True, null=True)
    correspondent_account = models.CharField(max_length=128, blank=True, null=True)
    payment_account = models.CharField(max_length=128, blank=True, null=True)

    def __str__(self):
        return self.company.company_name


##########################################################################################

# class ServiceCategory(models.Model):
#     name = models.CharField(max_length=32, blank=True, null=True)
#
#     def __str__(self):
#         return self.name


class ServiceSubCategory(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=True, null=True,
                                 related_name='sub_category', default=None)
    name = models.CharField(max_length=32, blank=True, null=True)

    def __str__(self):
        return self.name


class Service(models.Model):
    service_sub_category = models.ForeignKey(ServiceSubCategory, on_delete=models.CASCADE, related_name='sub_category', blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    employer = models.ForeignKey(User, on_delete=models.CASCADE, blank=True,
                                 null=True, related_name='services', default=None)


    def __str__(self):
        return self.service_sub_category.name
