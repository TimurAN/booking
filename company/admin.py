from django.contrib import admin
from modeltranslation.admin import TranslationAdmin
from company.models import *


class CompanyAdmin(admin.ModelAdmin):
    pass


class CompanyTranslatedEntryAdmin(CompanyAdmin, TranslationAdmin):
    pass


class ContactAdmin(admin.ModelAdmin):
    pass


class ContactTranslatedEntryAdmin(ContactAdmin, TranslationAdmin):
    pass


class DescriptionAdmin(admin.ModelAdmin):
    pass


class DescriptionTranslatedEntryAdmin(DescriptionAdmin, TranslationAdmin):
    pass


class RequisiteAdmin(admin.ModelAdmin):
    pass


class RequisiteTranslatedEntryAdmin(RequisiteAdmin, TranslationAdmin):
    pass


class EmployerServiceAdmin(admin.ModelAdmin):
    pass


# class ServiceCategoryAdmin(admin.ModelAdmin):
#     pass


# class ServiceCategoryTranslatedEntryAdmin(ServiceCategoryAdmin, TranslationAdmin):
#     pass


class ServiceSubCategoryAdmin(admin.ModelAdmin):
    pass


class ServiceSubCategoryTranslatedEntryAdmin(ServiceSubCategoryAdmin, TranslationAdmin):
    pass


admin.site.register(Service, EmployerServiceAdmin)
# admin.site.register(ServiceCategory, ServiceCategoryTranslatedEntryAdmin)
admin.site.register(ServiceSubCategory, ServiceSubCategoryTranslatedEntryAdmin)
admin.site.register(Contact, ContactTranslatedEntryAdmin)
admin.site.register(Company, CompanyTranslatedEntryAdmin)
admin.site.register(Description, DescriptionTranslatedEntryAdmin)
admin.site.register(Requisite, RequisiteTranslatedEntryAdmin)
admin.site.register(Image)
