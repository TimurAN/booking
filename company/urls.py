"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from company.views import *

urlpatterns = [
    path('calendar/all', CompanyCalendarView.as_view(), name='company'),
    path('calendar/employer/<str:emp_id>/', CompanyCalendarByEmployerView.as_view(), name='company-calendar-employer'),
    path('employer/', CompanyEmployerView.as_view(), name='company_employer_list'),
    path('meet/', CompanyMeetView.as_view(), name='company-meet'),
    path('create/employer/', CompanyCreateEmployerView.as_view(), name='company_create_employer'),
    path('update/employer/<int:employer_id>/', CompanyEmployerSettingsView.as_view(), name='company_update_employer'),
    path('delete/employer/image/<int:employer_id>/', DeleteEmployerImageView.as_view(), name='delete_employer_image'),

    # path('service-employer/', CompanyServiceEmployerView.as_view(), name='company-service-employer'),
    # path('settings/graphic-employer/', CompanyGraphicEmployerView.as_view(), name='company-graphic-employer'),
    # path('weekend-employer/', CompanyWeekendEmployerView.as_view(), name='company-weekend-employer'),
    path('customers/', CompanyCustomersView.as_view(), name='company-customers'),
    path('settings/main/', CompanyMainView.as_view(), name='create_company'),
    path('settings/information/contacts/', CompanyInformationContactView.as_view(),
         name='company-information-contacts'),
    path('settings/information/description/', CompanyInformationDescriptionView.as_view(),
         name='company-information-description'),
    path('settings/information/photo/', CompanyInformationPhotoView.as_view(), name='company-information-photo'),
    path('settings/information/requisite/', CompanyInformationRequisiteView.as_view(),
         name='company-information-requisite'),
    path('services/', CompanyServicesView.as_view(), name='company-services'),
    path('settings/schedule/', CompanyScheduleView.as_view(), name='company-schedule'),

]
