from modeltranslation.translator import register, TranslationOptions
from .models import *


@register(Company)
class CompanyTranslationOptions(TranslationOptions):
    fields = ('company_name', 'country', 'city')


@register(Contact)
class ContactTranslationOptions(TranslationOptions):
    fields = ('address', 'region')


@register(Description)
class DescriptionTranslationOptions(TranslationOptions):
    fields = ('description',)


@register(Requisite)
class RequisiteTranslationOptions(TranslationOptions):
    fields = ('legal_address', 'actual_address')

#
# @register(ServiceCategory)
# class RequisiteTranslationOptions(TranslationOptions):
#     fields = ('name',)


@register(ServiceSubCategory)
class RequisiteTranslationOptions(TranslationOptions):
    fields = ('name',)
