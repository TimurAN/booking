import datetime

import pytz
from django.core.exceptions import ValidationError

from company.models import Service
from employer.models import EmployerDayMode, EmployerHoliday


def create_service(user, form):
    for key in form:
        if key.__contains__('sub_category'):
            sub_category = form[key]
            price = form[sub_category]
            try:
                Service.objects.create(service_sub_category_id=sub_category, employer=user, price=price)
            except:
                raise ValidationError('Can you fill all required field please!!! Writened by Aman (may be you forget '
                                      'to fill price of service)')


def create_graphic(user, form):
    for key in form:
        if key.__contains__('check_'):
            day = key[-1]
            key = key.replace('check_', '')
            key = key.replace(day, '')
            start_work = form[f'{key}start_work']
            end_work = form[f'{key}end_work']
            start_break = form[f'{key}start_break']
            end_break = form[f'{key}end_break']
            is_break = False
            if f'{key}break' in form:
                is_break = True

            EmployerDayMode.objects.create(
                employer=user,
                working_day_is_active=True,
                day=day,
                start_work=start_work,
                end_work=end_work,
                is_break=is_break,
                start_break=start_break,
                end_break=end_break,
            )


def create_not_working_days(user, form):
    employer_holidays = []
    h = EmployerHoliday.objects.filter(employer=user)\

    if h:
        h.delete()



    for key in form:
        if key.__contains__('holiday'):
            date = form[key]
            date = datetime.datetime.strptime(date, "%Y-%m-%d").date()
            holiday = EmployerHoliday(date=date, employer=user)
            employer_holidays.append(holiday)


    EmployerHoliday.objects.bulk_create(employer_holidays)
