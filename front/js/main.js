$('.com-card__info').click(function() {
    $('.modal').addClass('active');
});
$('.modal-close').click(function() {
    $('.modal').removeClass('active');
});
$('.burger').click(function() {
    $('.aside').addClass('active');
});
$('.burger-x').click(function() {
    $('.aside').removeClass('active');
});
if ($('*').hasClass("calendar")) {
  $('#calendar').datepicker({
    firstDay: 0,
    weekends: [0],
    inline: true,
    showOtherMonths: false,
});
}
jQuery('.sign_in_btn').click(function () {
    jQuery('.popup').removeClass('active');
    jQuery('.sign-in,.black-overlay').addClass('active');
});
jQuery('.sign_up_btn').click(function () {
    jQuery('.popup').removeClass('active');
    jQuery('.sign-up,.black-overlay').addClass('active');
});
jQuery('.reset_pass_btn').click(function () {
    jQuery('.popup').removeClass('active');
    jQuery('.reset-pass,.black-overlay').addClass('active');
});
jQuery('.black-overlay').click(function () {
    jQuery('.popup').removeClass('active');
    jQuery(this).removeClass('active');
});
$('.set-time h2').click(function() {
    $(this).toggleClass('active');
});
$('.set-time h2').click(function() {
    $(this).next('.js-set-time').slideToggle();
});
if ($('*').hasClass("select")) {
	$('.select').niceSelect();
}
$('.user-modal__cap.js').click(function() {
    $(this).toggleClass('active');
});
$('.user-modal__cap.js').click(function() {
    $(this).next('.js-user').slideToggle();
});