-- MySQL dump 10.13  Distrib 8.0.17, for osx10.14 (x86_64)
--
-- Host: localhost    Database: booking
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add category',7,'add_category'),(26,'Can change category',7,'change_category'),(27,'Can delete category',7,'delete_category'),(28,'Can view category',7,'view_category'),(29,'Can add company',8,'add_company'),(30,'Can change company',8,'change_company'),(31,'Can delete company',8,'delete_company'),(32,'Can view company',8,'view_company'),(33,'Can add service category',9,'add_servicecategory'),(34,'Can change service category',9,'change_servicecategory'),(35,'Can delete service category',9,'delete_servicecategory'),(36,'Can view service category',9,'view_servicecategory'),(37,'Can add service',10,'add_service'),(38,'Can change service',10,'change_service'),(39,'Can delete service',10,'delete_service'),(40,'Can view service',10,'view_service'),(41,'Can add requisite',11,'add_requisite'),(42,'Can change requisite',11,'change_requisite'),(43,'Can delete requisite',11,'delete_requisite'),(44,'Can view requisite',11,'view_requisite'),(45,'Can add image',12,'add_image'),(46,'Can change image',12,'change_image'),(47,'Can delete image',12,'delete_image'),(48,'Can view image',12,'view_image'),(49,'Can add description',13,'add_description'),(50,'Can change description',13,'change_description'),(51,'Can delete description',13,'delete_description'),(52,'Can view description',13,'view_description'),(53,'Can add contact',14,'add_contact'),(54,'Can change contact',14,'change_contact'),(55,'Can delete contact',14,'delete_contact'),(56,'Can view contact',14,'view_contact'),(57,'Can add booking',15,'add_booking'),(58,'Can change booking',15,'change_booking'),(59,'Can delete booking',15,'delete_booking'),(60,'Can view booking',15,'view_booking'),(61,'Can add booking date',16,'add_bookingdate'),(62,'Can change booking date',16,'change_bookingdate'),(63,'Can delete booking date',16,'delete_bookingdate'),(64,'Can view booking date',16,'view_bookingdate'),(65,'Can add favorite',17,'add_favorite'),(66,'Can change favorite',17,'change_favorite'),(67,'Can delete favorite',17,'delete_favorite'),(68,'Can view favorite',17,'view_favorite'),(69,'Can add day',18,'add_day'),(70,'Can change day',18,'change_day'),(71,'Can delete day',18,'delete_day'),(72,'Can view day',18,'view_day'),(73,'Can add profile',19,'add_profile'),(74,'Can change profile',19,'change_profile'),(75,'Can delete profile',19,'delete_profile'),(76,'Can view profile',19,'view_profile'),(77,'Can add employer day mode',20,'add_employerdaymode'),(78,'Can change employer day mode',20,'change_employerdaymode'),(79,'Can delete employer day mode',20,'delete_employerdaymode'),(80,'Can view employer day mode',20,'view_employerdaymode'),(81,'Can add service sub category',21,'add_servicesubcategory'),(82,'Can change service sub category',21,'change_servicesubcategory'),(83,'Can delete service sub category',21,'delete_servicesubcategory'),(84,'Can view service sub category',21,'view_servicesubcategory'),(85,'Can add employer holiday',22,'add_employerholiday'),(86,'Can change employer holiday',22,'change_employerholiday'),(87,'Can delete employer holiday',22,'delete_employerholiday'),(88,'Can view employer holiday',22,'view_employerholiday'),(89,'Can add period',23,'add_period'),(90,'Can change period',23,'change_period'),(91,'Can delete period',23,'delete_period'),(92,'Can view period',23,'view_period');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (55,'pbkdf2_sha256$150000$ng93URTv2lQM$x073wSualbIdekZQD2g4pnXeu859EZsgLxQXn69KFBs=','2020-01-26 13:33:28.420887',1,'admin','','','koldoshbektashiev@gmail.com',1,1,'2020-01-22 07:33:29.361580'),(56,'pbkdf2_sha256$150000$QVHneXpAvNqJ$R4NOEHIhI4RjaAgSPjLB+h/c3Pf/jO+fS0qRMe8Q8jA=',NULL,1,'timur','','','timur@gmail.com',1,1,'2020-01-22 07:42:48.748949'),(57,'pbkdf2_sha256$150000$ZoRm4a1WLj8u$FuSA6e7Msfki9cWbVjs0qdulouzbatIwMMu4fSGVbzo=','2020-01-22 11:19:22.660047',1,'aman','','','aman@gmail.com',1,1,'2020-01-22 07:43:05.706132'),(58,'pbkdf2_sha256$150000$pAWDixefvoSV$nxS+/Z+WXHm9ykHROTRHeCMrxCIVTmZjHmhAt78a2Tc=','2020-01-22 08:45:22.338263',1,'koldoshbek','','','koldoshbektashiev@gmail.com',1,1,'2020-01-22 07:43:35.656341'),(59,'pbkdf2_sha256$150000$yusYr3LYwfyD$N/dKjHPFczKmybCvAKLkaMR4Cz2yQZVFyWvF2vO3RFE=','2020-01-22 09:58:40.890491',1,'askar','','','askar@gmail.com',1,1,'2020-01-22 07:46:04.911530'),(60,'pbkdf2_sha256$150000$8W8HBmaUh553$Xr6Fks+tTxqS4bKqE0F7fOvwlOP4+3005SR8dpLu50I=','2020-01-22 11:39:37.621750',1,'kesha','','','kesha@gmail.com',1,1,'2020-01-22 07:48:08.728170'),(61,'pbkdf2_sha256$150000$DfJ7hQl882YH$q6pJcXnLt0DDZJydrO6bljPpWBBLatx+SN+5gS8zlZc=',NULL,0,'+996778456532','','','',0,1,'2020-01-22 08:05:20.214454'),(62,'pbkdf2_sha256$150000$De5lhQs73Vvr$gmELoAzBApJdgrH2j/xEzps42On/mK17qLiEBWaBwyU=',NULL,0,'+996771545685','','','',0,1,'2020-01-22 08:10:05.148072'),(64,'pbkdf2_sha256$150000$B3k028p8tPUA$bgAMTB7ogxZiXZsNgHJdPXrkulrVZOZ+hcWVqLlahss=',NULL,0,'+996771311565','','','',0,1,'2020-01-22 08:11:19.235422'),(65,'pbkdf2_sha256$150000$G6gkUrwbKn4a$WtX8UOXDvG1em3TDRawufDJrx0/vq91p/Bk+GqwP+1Q=','2020-01-26 12:38:25.247243',0,'+996771895478','','','',0,1,'2020-01-22 08:14:37.924716'),(66,'pbkdf2_sha256$150000$xZoecBrRMSmY$SjU+G4SS8KLxOpK5FOoD4Iw3uCwZx5aUVPlSgnvxTsA=',NULL,0,'+996773717754','','','',0,1,'2020-01-22 08:53:04.697255'),(67,'pbkdf2_sha256$150000$imxZIOldHukL$IVvehWRbjykkWLCMUXRBpkjmgRIlU1abpsFiJC5skY8=',NULL,0,'+996771548965','','','',0,1,'2020-01-22 08:57:07.481492'),(68,'pbkdf2_sha256$150000$Jdx3C0HdMSXQ$X9v87QMI6887Eeals0QE9emQEqBr/3jCTlNqNqXVq2M=',NULL,0,'+996773547139','','','',0,1,'2020-01-22 09:00:04.188705'),(69,'pbkdf2_sha256$150000$EePhpAJ0JLko$h7XpSv2rQskQxlDPq19vEX3oooTzWH4+srWvgoz87tk=',NULL,0,'+996701626702','','','',0,1,'2020-01-22 10:02:01.990286'),(70,'pbkdf2_sha256$150000$pDUcdDBv7n9j$wEpUovTW9fpR+aAWIwUFIUU9DKCAxAO0E7g8njdbf1s=',NULL,0,'+996776459719','','','',0,1,'2020-01-23 08:52:38.121761');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_category`
--

DROP TABLE IF EXISTS `category_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title_ru` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_he` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_category`
--

LOCK TABLES `category_category` WRITE;
/*!40000 ALTER TABLE `category_category` DISABLE KEYS */;
INSERT INTO `category_category` VALUES (3,'Барбер Шоп','Барбер Шоп','Barber shop','Barber shop','image_of_categories/barber.jpg'),(4,'IT-Company','IT-Company','IT-Company','IT-Company','image_of_categories/itcpm.jpg');
/*!40000 ALTER TABLE `category_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_company`
--

DROP TABLE IF EXISTS `company_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_time` datetime(6) NOT NULL,
  `country` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `city_en` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_he` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_ru` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name_en` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name_he` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name_ru` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_en` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_he` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_ru` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `closed_time` time(6) DEFAULT NULL,
  `opened_time` time(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_company_user_id_c99db68c_uniq` (`user_id`),
  KEY `company_company_category_id_1eed524d_fk_category_category_id` (`category_id`),
  CONSTRAINT `company_company_category_id_1eed524d_fk_category_category_id` FOREIGN KEY (`category_id`) REFERENCES `category_category` (`id`),
  CONSTRAINT `company_company_user_id_c99db68c_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_company`
--

LOCK TABLES `company_company` WRITE;
/*!40000 ALTER TABLE `company_company` DISABLE KEYS */;
INSERT INTO `company_company` VALUES (5,'Барбер Маршал','2020-01-22 07:51:39.000000','Кыргызстан','Бишкек','logo_of_companies/ифкиукырщз.jpg',1,3,55,'Bishkek','Bishkek','Бишкек','Barber Marshal','Barber Marshal','Барбер Маршал','Kyrgyzstan','Kyrgyzstan','Кыргызстан','18:00:00.000000','08:00:00.000000'),(6,'Oracle Digital','2020-01-22 08:29:06.000000','Kyrgyzstan','Bishkek','logo_of_companies/oracle.png',0,4,59,'Bishkek','Bishkek','Бишкек','Oracle Digital','Oracle Digital','Oracle Digital','Kyrgyzstan','Kyrgyzstan','Кыргызстан','18:00:00.000000','08:00:00.000000'),(7,'Zensoft IO','2020-01-22 08:47:18.385884','Kyrgyzstan','Bishkek','logo_of_companies/zensoft.png',0,4,58,'Bishkek','Bishkek','Бишкек','Zensoft IO','Zensoft IO','Zensoft IO','Kyrgyzstan','Kyrgyzstan','Кыргызстан','18:00:00.000000','08:00:00.000000');
/*!40000 ALTER TABLE `company_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_contact`
--

DROP TABLE IF EXISTS `company_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_ru` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_en` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_he` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `index` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_ru` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_en` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_he` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `schedule` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `employer_id` int(11) DEFAULT NULL,
  `latitude` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_id` (`company_id`),
  UNIQUE KEY `employer_id` (`employer_id`),
  CONSTRAINT `company_contact_company_id_852620e3_fk_company_company_id` FOREIGN KEY (`company_id`) REFERENCES `company_company` (`id`),
  CONSTRAINT `company_contact_employer_id_5c5041c3_fk_auth_user_id` FOREIGN KEY (`employer_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_contact`
--

LOCK TABLES `company_contact` WRITE;
/*!40000 ALTER TABLE `company_contact` DISABLE KEYS */;
INSERT INTO `company_contact` VALUES (3,'Малдыбаева 45','Малдыбаева 45','Maldybaeva 45','Maldybaeva 45','754548','Ленин','Ленин','Lenin','Lenin 11','+996774568985','07:00-17:00',5,55,'42.872831','74.654522'),(4,'Тимирязева 60','Тимирязева 60','Timiryazeva 60','Timiryazeva 60','754548','Ленин','Ленин','Lenin','Lenin','+996774568985','09:00-17:00',6,59,'42.707669','74.674356'),(5,'Малдыбаева 45','Малдыбаева 45','Maldybaeva 45','Maldybaeva 45','754548','Ленин','Ленин','Lenin','Lenin','+996774568985','07:00-17:00',7,58,'42.707669','74.474356');
/*!40000 ALTER TABLE `company_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_description`
--

DROP TABLE IF EXISTS `company_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description_ru` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `description_en` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `description_he` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `company_id` int(11) DEFAULT NULL,
  `employer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_id` (`company_id`),
  UNIQUE KEY `employer_id` (`employer_id`),
  CONSTRAINT `company_description_company_id_ad2d1b07_fk_company_company_id` FOREIGN KEY (`company_id`) REFERENCES `company_company` (`id`),
  CONSTRAINT `company_description_employer_id_7e40ebe8_fk_auth_user_id` FOREIGN KEY (`employer_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_description`
--

LOCK TABLES `company_description` WRITE;
/*!40000 ALTER TABLE `company_description` DISABLE KEYS */;
INSERT INTO `company_description` VALUES (3,'Hello world','Привет Мир','Hello world','Hello world',5,NULL),(4,'Hello','Hello','Hello','Hello',6,NULL),(5,'Hello','Zensoft IO','Hello','Zensoft IO',7,NULL);
/*!40000 ALTER TABLE `company_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_image`
--

DROP TABLE IF EXISTS `company_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `employer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_image_company_id_5acd5194_fk_company_company_id` (`company_id`),
  KEY `company_image_employer_id_2af2f368_fk_auth_user_id` (`employer_id`),
  CONSTRAINT `company_image_company_id_5acd5194_fk_company_company_id` FOREIGN KEY (`company_id`) REFERENCES `company_company` (`id`),
  CONSTRAINT `company_image_employer_id_2af2f368_fk_auth_user_id` FOREIGN KEY (`employer_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_image`
--

LOCK TABLES `company_image` WRITE;
/*!40000 ALTER TABLE `company_image` DISABLE KEYS */;
INSERT INTO `company_image` VALUES (1,'image_of_companies/ифкиукырщз.jpg',5,60),(2,'image_of_companies/oracle.png',6,59),(3,'image_of_companies/zensoft.png',7,58);
/*!40000 ALTER TABLE `company_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_requisite`
--

DROP TABLE IF EXISTS `company_requisite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company_requisite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `legal_entity` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `inn` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `kpp` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bik` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `legal_address` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `legal_address_ru` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `legal_address_en` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `legal_address_he` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `actual_address` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `actual_address_ru` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `actual_address_en` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `actual_address_he` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `correspondent_account` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_account` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `employer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_id` (`company_id`),
  UNIQUE KEY `employer_id` (`employer_id`),
  CONSTRAINT `company_requisite_company_id_828bbea2_fk_company_company_id` FOREIGN KEY (`company_id`) REFERENCES `company_company` (`id`),
  CONSTRAINT `company_requisite_employer_id_975678d4_fk_auth_user_id` FOREIGN KEY (`employer_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_requisite`
--

LOCK TABLES `company_requisite` WRITE;
/*!40000 ALTER TABLE `company_requisite` DISABLE KEYS */;
INSERT INTO `company_requisite` VALUES (2,'ОсОО','Hellow','111800000545454','11180000545654','118545689','Maldybaeva 45','Малдыбаева 45','Maldybaeva 45','Maldybaeva 45','Maldybaeva 45','Малдыбаева 45','Maldybaeva 45','Maldybaeva 45','Demir Bank','11800000653245','11800000456585',5,60),(3,'ОсОО','Askar','111800000545454','11800000548987','11800000654565','Maldybaeva 45','Малдыбаева 45','Maldybaeva 45','Maldybaeva 45','Maldybaeva 45','Малдыбаева 45','Maldybaeva 45','Maldybaeva 45','Demir Bank','11800000653245','11800000456585',6,59),(4,'ОсОО','Koldoshbek','111800000545454','11800000548987','11800000654565','Maldybaeva 45','Малдыбаева 45','Maldybaeva 45','Maldybaeva 45','Maldybaeva 45','Малдыбаева 45','Maldybaeva 45','Maldybaeva 45','Demir Bank','11800000653245','11800000456585',7,58);
/*!40000 ALTER TABLE `company_requisite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_service`
--

DROP TABLE IF EXISTS `company_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` int(11) DEFAULT NULL,
  `service_sub_category_id` int(11) DEFAULT NULL,
  `employer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_service_service_sub_category_e1445926_fk_company_s` (`service_sub_category_id`),
  KEY `company_service_employer_id_8d8eb66c_fk_auth_user_id` (`employer_id`),
  CONSTRAINT `company_service_employer_id_8d8eb66c_fk_auth_user_id` FOREIGN KEY (`employer_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `company_service_service_sub_category_e1445926_fk_company_s` FOREIGN KEY (`service_sub_category_id`) REFERENCES `company_servicesubcategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_service`
--

LOCK TABLES `company_service` WRITE;
/*!40000 ALTER TABLE `company_service` DISABLE KEYS */;
INSERT INTO `company_service` VALUES (20,300,11,61),(21,200,12,61),(22,150,14,61),(23,500,10,64),(24,500,11,64),(25,300,12,64),(26,1200,13,64),(27,400,10,65),(28,500,11,65),(29,200,12,65),(30,600,13,65),(31,100,14,65),(32,300,15,66),(33,500,23,66),(34,1200,24,66),(35,300,25,66),(36,100,26,66),(37,300,15,67),(38,1500,16,67),(39,1500,17,67),(40,800,18,67),(41,1500,19,67),(42,800,20,67),(43,1500,21,67),(44,2500,22,67),(45,500,23,67),(46,400,24,67),(47,100,25,67),(48,100,26,67),(49,1200,16,68),(50,700,17,68),(51,300,18,68),(52,1020,19,68),(53,1500,21,68),(54,1500,22,68),(55,350,23,68),(56,300,15,69),(57,1200,16,69),(58,600,17,69),(59,1500,21,69),(60,1200,22,69),(61,400,23,69),(62,300,10,70),(63,150,12,70),(64,200,11,55);
/*!40000 ALTER TABLE `company_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_servicesubcategory`
--

DROP TABLE IF EXISTS `company_servicesubcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company_servicesubcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ru` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_he` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_servicesubca_category_id_ba26da84_fk_category_` (`category_id`),
  CONSTRAINT `company_servicesubca_category_id_ba26da84_fk_category_` FOREIGN KEY (`category_id`) REFERENCES `category_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_servicesubcategory`
--

LOCK TABLES `company_servicesubcategory` WRITE;
/*!40000 ALTER TABLE `company_servicesubcategory` DISABLE KEYS */;
INSERT INTO `company_servicesubcategory` VALUES (10,'Hair Styling','Укладка волос','Hair Styling','Hair Styling',3),(11,'Men\'s haircut','Мужская стрижка','Men\'s haircut','Men\'s haircut',3),(12,'Baby haircut','Детская стрижка','Baby haircut','Baby haircut',3),(13,'Dangerous Shave','Опасное бритья','Dangerous Shave','Dangerous Shave',3),(14,'Beard haircut','Стрижка бороды','Beard haircut','Beard haircut',3),(15,'LANDING PAGE','LANDING PAGE','LANDING PAGE','LANDING PAGE',4),(16,'Online store','Интернет магазин','Online store','Online store',4),(17,'Business card website','Сайт визитка','Business card website','Business card website',4),(18,'Corporate website','Корпоративный сайт','Corporate website','Corporate website',4),(19,'IOS & Android Apps','Приложения под IOS & Android','IOS & Android Apps','IOS & Android Apps',4),(20,'Portals and Services','Порталы и сервисы','Portals and Services','Portals and Services',4),(21,'Business automation','Автоматизация бизнеса','Business automation','Business automation',4),(22,'CRM','CRM','CRM','CRM',4),(23,'Sites for network companies','Сайты для сетевых компаний','Sites for network companies','Sites for network companies',4),(24,'Blockchain-based web platforms','Веб-платформы на базе Blockchain','Blockchain-based web platforms','Blockchain-based web platforms',4),(25,'Site Adjustment','Корректировка сайта','Site Adjustment','Site Adjustment',4),(26,'Telegram Bots','Телеграм боты','Telegram Bots','Telegram Bots',4);
/*!40000 ALTER TABLE `company_servicesubcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_booking`
--

DROP TABLE IF EXISTS `customer_booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime(6) NOT NULL,
  `status` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `employer_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `booking_date` date NOT NULL,
  `contact_comments` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_booking_company_id_5e36c3ad_fk_company_company_id` (`company_id`),
  KEY `customer_booking_customer_id_622c38ea_fk_auth_user_id` (`customer_id`),
  KEY `customer_booking_employer_id_654f16c5_fk_auth_user_id` (`employer_id`),
  KEY `customer_booking_service_id_a2f74424_fk_company_service_id` (`service_id`),
  CONSTRAINT `customer_booking_company_id_5e36c3ad_fk_company_company_id` FOREIGN KEY (`company_id`) REFERENCES `company_company` (`id`),
  CONSTRAINT `customer_booking_customer_id_622c38ea_fk_auth_user_id` FOREIGN KEY (`customer_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `customer_booking_employer_id_654f16c5_fk_auth_user_id` FOREIGN KEY (`employer_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `customer_booking_service_id_a2f74424_fk_company_service_id` FOREIGN KEY (`service_id`) REFERENCES `company_service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_booking`
--

LOCK TABLES `customer_booking` WRITE;
/*!40000 ALTER TABLE `customer_booking` DISABLE KEYS */;
INSERT INTO `customer_booking` VALUES (25,'2020-01-22 09:08:51.464143','В ожидании',5,65,65,20,'2020-01-10','Hello','koldoshbektashiev@gmail.com','Koldoshbek','+996771872351'),(26,'2020-01-22 10:05:26.400737','В ожидании',6,69,69,44,'2020-01-24','sad','koldoshbektashiev@gmail.com','TImur','+99678454452'),(27,'2020-01-23 09:12:30.435192','В ожидании',5,60,64,20,'2020-01-08','s','koldoshbektashiev@gmail.com','Koldoshbek','+99678454452'),(28,'2020-01-26 12:20:02.509739','В ожидании',5,55,65,22,'2020-01-27','ohooo','zyntemirov@gmail.com','Azamat','0705050035');
/*!40000 ALTER TABLE `customer_booking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_favorite`
--

DROP TABLE IF EXISTS `customer_favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer_favorite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `add_date` datetime(6) NOT NULL,
  `company_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_favorite_company_id_2c4f320d_fk_company_company_id` (`company_id`),
  KEY `customer_favorite_customer_id_33343edb_fk_user_profile_id` (`customer_id`),
  CONSTRAINT `customer_favorite_company_id_2c4f320d_fk_company_company_id` FOREIGN KEY (`company_id`) REFERENCES `company_company` (`id`),
  CONSTRAINT `customer_favorite_customer_id_33343edb_fk_user_profile_id` FOREIGN KEY (`customer_id`) REFERENCES `user_profile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_favorite`
--

LOCK TABLES `customer_favorite` WRITE;
/*!40000 ALTER TABLE `customer_favorite` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_favorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_period`
--

DROP TABLE IF EXISTS `customer_period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer_period` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_period` time(6) NOT NULL,
  `end_period` time(6) NOT NULL,
  `booking_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_period_booking_id_651951f7_fk_customer_booking_id` (`booking_id`),
  CONSTRAINT `customer_period_booking_id_651951f7_fk_customer_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `customer_booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_period`
--

LOCK TABLES `customer_period` WRITE;
/*!40000 ALTER TABLE `customer_period` DISABLE KEYS */;
INSERT INTO `customer_period` VALUES (16,'09:00:00.000000','09:30:00.000000',25),(17,'09:30:00.000000','10:00:00.000000',25),(18,'12:00:00.000000','12:30:00.000000',26),(19,'12:30:00.000000','13:00:00.000000',26),(20,'13:00:00.000000','13:30:00.000000',27),(21,'11:00:00.000000','11:30:00.000000',28);
/*!40000 ALTER TABLE `customer_period` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `object_repr` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (135,'2020-01-22 07:34:00.328216','2','Beauty_Saloon',3,'',7,55),(136,'2020-01-22 07:34:00.330978','1','Hairdresser',3,'',7,55),(137,'2020-01-22 07:41:04.248321','3','Barbershop',1,'[{\"added\": {}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u0423\\u043a\\u043b\\u0430\\u0434\\u043a\\u0430 \\u0432\\u043e\\u043b\\u043e\\u0441\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u041c\\u0443\\u0436\\u0441\\u043a\\u0430\\u044f \\u0441\\u0442\\u0440\\u0438\\u0436\\u043a\\u0430\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u0414\\u0435\\u0442\\u0441\\u043a\\u0430\\u044f \\u0441\\u0442\\u0440\\u0438\\u0436\\u043a\\u0430\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u041e\\u043f\\u0430\\u0441\\u043d\\u043e\\u0435 \\u0431\\u0440\\u0438\\u0442\\u044c\\u044f\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u0421\\u0442\\u0440\\u0438\\u0436\\u043a\\u0430 \\u0431\\u043e\\u0440\\u043e\\u0434\\u044b\"}}]',7,55),(138,'2020-01-22 07:44:45.245067','55','admin',2,'[{\"changed\": {\"name\": \"profile\", \"object\": \"admin\", \"fields\": [\"name\", \"email\", \"country\", \"city\", \"information\"]}}]',4,55),(139,'2020-01-22 07:45:48.658168','58','koldoshbek',2,'[{\"changed\": {\"name\": \"profile\", \"object\": \"koldoshbek\", \"fields\": [\"name\", \"email\", \"phone\", \"country\", \"city\", \"information\"]}}]',4,55),(140,'2020-01-22 07:47:05.957636','59','askar',2,'[{\"changed\": {\"name\": \"profile\", \"object\": \"askar\", \"fields\": [\"name\", \"email\", \"is_company\", \"country\", \"city\", \"information\"]}}]',4,55),(141,'2020-01-22 07:49:17.188561','60','kesha',2,'[{\"changed\": {\"name\": \"profile\", \"object\": \"kesha\", \"fields\": [\"name\", \"email\", \"is_company\", \"phone\", \"country\", \"city\", \"information\"]}}]',4,59),(142,'2020-01-22 08:00:31.560577','14','Beard haircut',2,'[{\"changed\": {\"fields\": [\"name_en\", \"name_he\"]}}]',21,60),(143,'2020-01-22 08:01:10.883254','13','Dangerous Shave',2,'[{\"changed\": {\"fields\": [\"name_en\", \"name_he\"]}}]',21,60),(144,'2020-01-22 08:01:32.244876','12','Baby haircut',2,'[{\"changed\": {\"fields\": [\"name_en\", \"name_he\"]}}]',21,60),(145,'2020-01-22 08:01:50.997225','11','Men\'s haircut',2,'[{\"changed\": {\"fields\": [\"name_en\", \"name_he\"]}}]',21,60),(146,'2020-01-22 08:02:03.543454','10','Hair Styling',2,'[]',21,60),(147,'2020-01-22 08:26:36.838707','4','IT-Company \"Oracle\"',1,'[{\"added\": {}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"LANDING PAGE\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u0418\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442 \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u0421\\u0430\\u0439\\u0442 \\u0432\\u0438\\u0437\\u0438\\u0442\\u043a\\u0430\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u041a\\u043e\\u0440\\u043f\\u043e\\u0440\\u0430\\u0442\\u0438\\u0432\\u043d\\u044b\\u0439 \\u0441\\u0430\\u0439\\u0442\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u041f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f \\u043f\\u043e\\u0434 IOS & Android\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u041f\\u043e\\u0440\\u0442\\u0430\\u043b\\u044b \\u0438 \\u0441\\u0435\\u0440\\u0432\\u0438\\u0441\\u044b\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u0410\\u0432\\u0442\\u043e\\u043c\\u0430\\u0442\\u0438\\u0437\\u0430\\u0446\\u0438\\u044f \\u0431\\u0438\\u0437\\u043d\\u0435\\u0441\\u0430\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"CRM\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u0421\\u0430\\u0439\\u0442\\u044b \\u0434\\u043b\\u044f \\u0441\\u0435\\u0442\\u0435\\u0432\\u044b\\u0445 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0439\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u0412\\u0435\\u0431-\\u043f\\u043b\\u0430\\u0442\\u0444\\u043e\\u0440\\u043c\\u044b \\u043d\\u0430 \\u0431\\u0430\\u0437\\u0435 Blockchain\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u041a\\u043e\\u0440\\u0440\\u0435\\u043a\\u0442\\u0438\\u0440\\u043e\\u0432\\u043a\\u0430 \\u0441\\u0430\\u0439\\u0442\\u0430\"}}, {\"added\": {\"name\": \"service sub category\", \"object\": \"\\u0422\\u0435\\u043b\\u0435\\u0433\\u0440\\u0430\\u043c \\u0431\\u043e\\u0442\\u044b\"}}]',7,59),(148,'2020-01-22 08:27:32.567189','4','IT-Company',2,'[{\"changed\": {\"fields\": [\"title_ru\", \"title_en\", \"title_he\"]}}]',7,59),(149,'2020-01-22 08:28:08.968736','4','IT-Company',2,'[{\"changed\": {\"fields\": [\"image\"]}}]',7,59),(150,'2020-01-22 08:45:34.999651','58','koldoshbek',2,'[{\"changed\": {\"name\": \"profile\", \"object\": \"koldoshbek\", \"fields\": [\"is_company\"]}}]',4,58),(151,'2020-01-22 09:55:31.988062','6','Oracle Digital',2,'[]',8,60),(152,'2020-01-22 11:22:28.106284','57','aman',2,'[{\"changed\": {\"name\": \"profile\", \"object\": \"aman\", \"fields\": [\"image\", \"country\", \"city\"]}}]',4,57),(153,'2020-01-22 11:25:47.767126','57','aman',2,'[{\"changed\": {\"name\": \"profile\", \"object\": \"aman\", \"fields\": [\"image\"]}}]',4,57),(154,'2020-01-23 11:56:31.550747','3','Барбер Шоп',2,'[{\"changed\": {\"fields\": [\"title_ru\", \"title_en\", \"title_he\"]}}]',7,55),(155,'2020-01-26 12:09:22.518153','55','admin',2,'[{\"changed\": {\"name\": \"profile\", \"object\": \"admin\", \"fields\": [\"is_company\", \"profession\"]}}]',4,55),(156,'2020-01-26 12:10:35.311740','55','admin',2,'[{\"changed\": {\"name\": \"profile\", \"object\": \"admin\", \"fields\": [\"company\"]}}]',4,55),(157,'2020-01-26 12:13:57.262016','55','admin',2,'[{\"changed\": {\"name\": \"profile\", \"object\": \"admin\", \"fields\": [\"is_company\"]}}]',4,55),(158,'2020-01-26 12:16:56.205141','5','Барбер Маршал',2,'[{\"changed\": {\"fields\": [\"is_active\"]}}]',8,55),(159,'2020-01-26 13:38:40.546882','5','Contact object (5)',2,'[{\"changed\": {\"fields\": [\"latitude\", \"longitude\"]}}]',14,55),(160,'2020-01-26 13:40:18.771216','3','Contact object (3)',2,'[{\"changed\": {\"fields\": [\"latitude\", \"longitude\"]}}]',14,55),(161,'2020-01-26 14:24:16.450483','4','Contact object (4)',2,'[{\"changed\": {\"fields\": [\"latitude\", \"longitude\"]}}]',14,55),(162,'2020-01-26 14:24:36.075318','4','Contact object (4)',2,'[]',14,55),(163,'2020-01-26 14:25:40.801997','5','Contact object (5)',2,'[{\"changed\": {\"fields\": [\"longitude\"]}}]',14,55);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(7,'category','category'),(8,'company','company'),(14,'company','contact'),(13,'company','description'),(12,'company','image'),(11,'company','requisite'),(10,'company','service'),(9,'company','servicecategory'),(21,'company','servicesubcategory'),(5,'contenttypes','contenttype'),(15,'customer','booking'),(16,'customer','bookingdate'),(18,'customer','day'),(17,'customer','favorite'),(23,'customer','period'),(20,'employer','employerdaymode'),(22,'employer','employerholiday'),(6,'sessions','session'),(19,'user','profile');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-12-27 11:07:12.961533'),(2,'auth','0001_initial','2019-12-27 11:07:13.138608'),(3,'admin','0001_initial','2019-12-27 11:07:13.564602'),(4,'admin','0002_logentry_remove_auto_add','2019-12-27 11:07:13.674689'),(5,'admin','0003_logentry_add_action_flag_choices','2019-12-27 11:07:13.689599'),(6,'contenttypes','0002_remove_content_type_name','2019-12-27 11:07:13.786077'),(7,'auth','0002_alter_permission_name_max_length','2019-12-27 11:07:13.845980'),(8,'auth','0003_alter_user_email_max_length','2019-12-27 11:07:13.897010'),(9,'auth','0004_alter_user_username_opts','2019-12-27 11:07:13.913912'),(10,'auth','0005_alter_user_last_login_null','2019-12-27 11:07:13.961239'),(11,'auth','0006_require_contenttypes_0002','2019-12-27 11:07:13.965152'),(12,'auth','0007_alter_validators_add_error_messages','2019-12-27 11:07:13.980125'),(13,'auth','0008_alter_user_username_max_length','2019-12-27 11:07:14.033254'),(14,'auth','0009_alter_user_last_name_max_length','2019-12-27 11:07:14.096684'),(15,'auth','0010_alter_group_name_max_length','2019-12-27 11:07:14.157413'),(16,'auth','0011_update_proxy_permissions','2019-12-27 11:07:14.173155'),(17,'category','0001_initial','2019-12-27 11:07:14.200013'),(18,'company','0001_initial','2019-12-27 11:07:14.546873'),(19,'user','0001_initial','2019-12-27 11:07:15.282014'),(20,'customer','0001_initial','2019-12-27 11:07:15.527176'),(21,'employer','0001_initial','2019-12-27 11:07:15.990614'),(22,'sessions','0001_initial','2019-12-27 11:07:16.078278'),(23,'user','0002_profile_company','2019-12-27 11:12:24.545323'),(24,'company','0002_auto_20191227_1120','2019-12-27 11:20:14.230126'),(25,'company','0003_auto_20191227_1126','2019-12-27 11:26:36.806915'),(26,'company','0004_auto_20191227_1631','2019-12-27 16:52:52.728151'),(27,'company','0005_auto_20191227_1944','2019-12-27 19:44:26.597036'),(28,'company','0006_auto_20191227_1958','2019-12-27 19:59:10.581158'),(29,'employer','0002_employerholiday','2019-12-27 22:25:48.942315'),(30,'company','0007_auto_20191228_1452','2019-12-28 14:55:10.313942'),(31,'customer','0002_auto_20191228_1452','2019-12-28 14:55:10.695917'),(32,'employer','0003_employerweekend','2019-12-28 14:55:10.801536'),(33,'employer','0004_auto_20191228_1454','2019-12-28 14:55:10.949030'),(34,'company','0008_auto_20191228_1737','2019-12-28 17:37:38.011269'),(35,'company','0009_remove_service_service_category','2019-12-28 17:43:22.622800'),(36,'user','0003_auto_20191228_2017','2019-12-28 20:17:22.339623'),(37,'employer','0005_auto_20191228_2135','2019-12-28 21:35:16.096481'),(38,'user','0004_auto_20191231_0951','2019-12-31 09:51:57.672049'),(39,'company','0010_auto_20200102_2103','2020-01-02 21:05:09.568441'),(40,'customer','0003_auto_20200102_2103','2020-01-02 21:05:09.806177'),(41,'user','0005_auto_20200102_2103','2020-01-02 21:05:10.131908'),(42,'customer','0004_auto_20200104_0149','2020-01-04 01:49:28.196364'),(43,'company','0011_auto_20200110_2209','2020-01-10 16:09:58.530766'),(44,'user','0006_auto_20200110_2209','2020-01-10 16:09:59.013138'),(45,'company','0012_auto_20200111_1722','2020-01-11 11:22:30.608928'),(46,'company','0002_auto_20200111_2217','2020-01-12 06:49:17.963839'),(47,'company','0002_auto_20200118_2139','2020-01-18 15:39:52.397690'),(48,'customer','0002_auto_20200118_2139','2020-01-18 15:39:52.615535'),(49,'user','0002_profile_profession','2020-01-23 11:55:00.914861'),(50,'company','0002_auto_20200123_1757','2020-01-23 11:57:40.572424'),(51,'company','0002_auto_20200120_1148','2020-01-26 12:05:22.080196'),(52,'company','0003_auto_20200126_1749','2020-01-26 12:05:22.083387'),(53,'customer','0002_auto_20200120_1148','2020-01-26 12:05:22.085636'),(54,'customer','0003_auto_20200126_1749','2020-01-26 12:05:22.088018'),(55,'user','0002_auto_20200120_1148','2020-01-26 12:05:22.090847'),(56,'user','0003_auto_20200126_1749','2020-01-26 12:05:22.093409'),(57,'company','0004_auto_20200126_1937','2020-01-26 13:37:27.397730');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `session_data` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('041yczq70d4c6mltp91tdl3ylrsbmx3g','MDZjZDU4Y2YxMDVmOWE3ZTc2MTQ3Nzg4ZjRjZDk0OWQ1MzNiMzUzNjp7Il9hdXRoX3VzZXJfaWQiOiI1NSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYzdjMDQ4MWZlMzQ2NmY3NTBjNDJiNTlmOWViNGNmZTBkMTQwZmFmMyJ9','2020-02-06 11:55:10.622319'),('47zzm3oug5tlqmrbig6kc0gl0u3rqrkv','NGMxMWE4MjRlYjViZjUzMjRkM2E0Mzk4YWFjZmExODFjODVhYjJkMzp7Il9hdXRoX3VzZXJfaWQiOiI2MCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYmIzNzQ3MmQxNTZjNzg1NDg2ZmRjM2Y0NDI1YmVkMjBhZmFkMjg4YyJ9','2020-02-05 11:39:37.626474'),('7prt534cm5aclk343eyz5jdliwo5iuer','ZTlkZjNlMTgzODFkYzEzYzVjMTQ5MTAyZjQzNGFjNjYwOWNjOTU2Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJmZWJmOTc0MDQwMmE3NjRiNTdhOGNiY2M2YmYxZDc0NWE5ZDJjMTJkIn0=','2020-01-11 17:19:04.063364'),('9idoeym8lnhlflr1oic6hh7ahal00dcx','ZmM0ODA2OTFjNGUyN2FkYTYxNzMxY2ZmNzRhMTcyMTNkYjliZTNiNzp7fQ==','2020-02-06 11:44:56.718367'),('ell013lf0q2ae7tyvrpx4kyd9vhx2sg7','ZTlkZjNlMTgzODFkYzEzYzVjMTQ5MTAyZjQzNGFjNjYwOWNjOTU2Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJmZWJmOTc0MDQwMmE3NjRiNTdhOGNiY2M2YmYxZDc0NWE5ZDJjMTJkIn0=','2020-01-14 15:10:30.949820'),('fvvs6j9cs9jkwxpfb18jmx5h28tjcxf1','ZmM0ODA2OTFjNGUyN2FkYTYxNzMxY2ZmNzRhMTcyMTNkYjliZTNiNzp7fQ==','2020-02-06 11:49:46.603835'),('m86c6xztqgtg6r0lebcl0uf82ks2my42','NjVlYWY4NjhkZTYzZGNhY2FiYTBjMWE2YmE1OTc0ZGJkNTc1Y2MzNTp7Il9sYW5ndWFnZSI6ImVuIiwiX2F1dGhfdXNlcl9pZCI6IjU1IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJjN2MwNDgxZmUzNDY2Zjc1MGM0MmI1OWY5ZWI0Y2ZlMGQxNDBmYWYzIn0=','2020-02-13 17:17:49.662635'),('n8jcamlp9kks3b1isvh464tgztsk5l2h','ZmM0ODA2OTFjNGUyN2FkYTYxNzMxY2ZmNzRhMTcyMTNkYjliZTNiNzp7fQ==','2020-02-06 11:50:13.600886'),('slf294kilc94x0t0nsjjqovvnxd69h68','ZTlkZjNlMTgzODFkYzEzYzVjMTQ5MTAyZjQzNGFjNjYwOWNjOTU2Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJmZWJmOTc0MDQwMmE3NjRiNTdhOGNiY2M2YmYxZDc0NWE5ZDJjMTJkIn0=','2020-01-25 21:01:45.640732'),('uj3ellhw80payeecybpchucc1pjcx3nb','ZjJhMzQ5MTU4ZDI0MGRjYjExMjVhMGQwZmNlMTk3ZWRiY2UyZmQ0NTp7Il9sYW5ndWFnZSI6InJ1IiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjM4NDIyZDUxMjg3NjQyZTI2NGEwNmIwYWVhYWIxYjY4MWJjMWRkNmQifQ==','2020-02-04 03:15:16.768447'),('vb6fvopf89tvetwwujwiu959nt3aql12','MmQzYWI1NzQ4NmViNmE2YjllYzgwZGFmNjY0ZTJlMDViOWQ4Y2Q2ZDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIzODQyMmQ1MTI4NzY0MmUyNjRhMDZiMGFlYWFiMWI2ODFiYzFkZDZkIn0=','2020-01-26 09:25:50.252641'),('x3ohn6aigo5g3xdihwp5qy9xdzbfkldr','ZmM0ODA2OTFjNGUyN2FkYTYxNzMxY2ZmNzRhMTcyMTNkYjliZTNiNzp7fQ==','2020-02-06 11:45:27.094680'),('xpns6qodhmp9p7kbo40zf7mm354g632w','MDZjZDU4Y2YxMDVmOWE3ZTc2MTQ3Nzg4ZjRjZDk0OWQ1MzNiMzUzNjp7Il9hdXRoX3VzZXJfaWQiOiI1NSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYzdjMDQ4MWZlMzQ2NmY3NTBjNDJiNTlmOWViNGNmZTBkMTQwZmFmMyJ9','2020-02-09 12:07:15.344209');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employer_employerdaymode`
--

DROP TABLE IF EXISTS `employer_employerdaymode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employer_employerdaymode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `working_day_is_active` tinyint(1) NOT NULL,
  `day` int(11) NOT NULL,
  `start_work` time(6) NOT NULL,
  `end_work` time(6) NOT NULL,
  `is_break` tinyint(1) NOT NULL,
  `start_break` time(6) NOT NULL,
  `end_break` time(6) NOT NULL,
  `employer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employer_employerdaymode_employer_id_day_971df81a_uniq` (`employer_id`,`day`),
  CONSTRAINT `employer_employerdaymode_employer_id_eb26039e_fk_auth_user_id` FOREIGN KEY (`employer_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employer_employerdaymode`
--

LOCK TABLES `employer_employerdaymode` WRITE;
/*!40000 ALTER TABLE `employer_employerdaymode` DISABLE KEYS */;
INSERT INTO `employer_employerdaymode` VALUES (66,1,0,'08:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',61),(67,1,1,'08:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',61),(68,1,2,'08:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',61),(69,1,3,'08:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',61),(70,1,4,'10:00:00.000000','16:00:00.000000',1,'13:00:00.000000','14:00:00.000000',61),(71,1,5,'10:00:00.000000','16:00:00.000000',1,'13:00:00.000000','14:00:00.000000',61),(72,1,0,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',64),(73,1,1,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',64),(74,1,2,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',64),(75,1,3,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',64),(76,1,4,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',64),(77,1,0,'09:00:00.000000','17:00:00.000000',1,'11:30:00.000000','12:30:00.000000',65),(78,1,1,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',65),(79,1,2,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',65),(80,1,3,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',65),(81,1,4,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','14:30:00.000000',65),(82,1,5,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',65),(83,1,0,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',66),(84,1,1,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',66),(85,1,2,'09:00:00.000000','12:00:00.000000',1,'12:00:00.000000','13:00:00.000000',66),(86,1,3,'08:30:00.000000','17:30:00.000000',1,'13:00:00.000000','14:00:00.000000',66),(87,1,4,'09:00:00.000000','17:00:00.000000',1,'13:00:00.000000','14:00:00.000000',66),(88,1,5,'09:00:00.000000','17:00:00.000000',1,'13:00:00.000000','14:00:00.000000',66),(89,1,6,'10:00:00.000000','19:00:00.000000',1,'12:30:00.000000','14:00:00.000000',66),(90,1,0,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',67),(91,1,1,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',67),(92,1,2,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',67),(93,1,3,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',67),(94,1,4,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',67),(95,1,5,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',67),(96,1,0,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',68),(97,1,1,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',68),(98,1,2,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',68),(99,1,3,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',68),(100,1,4,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',68),(101,1,0,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',69),(102,1,1,'10:00:00.000000','19:00:00.000000',1,'13:00:00.000000','14:00:00.000000',69),(103,1,2,'10:00:00.000000','19:00:00.000000',1,'13:00:00.000000','14:00:00.000000',69),(104,1,3,'10:00:00.000000','19:00:00.000000',1,'13:00:00.000000','14:00:00.000000',69),(105,1,4,'10:00:00.000000','19:00:00.000000',1,'13:00:00.000000','14:00:00.000000',69),(106,1,5,'09:00:00.000000','18:00:00.000000',1,'12:00:00.000000','13:00:00.000000',69),(107,1,0,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',70),(108,1,1,'09:00:00.000000','17:00:00.000000',1,'12:30:00.000000','13:30:00.000000',70),(109,1,2,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',70),(110,1,3,'09:00:00.000000','17:00:00.000000',1,'12:00:00.000000','13:00:00.000000',70),(111,1,4,'09:00:00.000000','17:00:00.000000',1,'13:00:00.000000','14:00:00.000000',70),(112,1,0,'06:00:00.000000','17:30:00.000000',0,'06:00:00.000000','06:00:00.000000',55),(113,1,1,'06:00:00.000000','22:30:00.000000',0,'06:00:00.000000','06:00:00.000000',55),(114,1,2,'06:00:00.000000','20:30:00.000000',0,'06:00:00.000000','06:00:00.000000',55),(115,1,3,'06:00:00.000000','17:00:00.000000',0,'06:00:00.000000','06:00:00.000000',55),(116,1,4,'06:00:00.000000','17:30:00.000000',0,'06:00:00.000000','06:00:00.000000',55),(117,1,5,'06:00:00.000000','21:00:00.000000',0,'06:00:00.000000','06:00:00.000000',55),(118,1,6,'06:00:00.000000','18:00:00.000000',0,'06:00:00.000000','06:00:00.000000',55);
/*!40000 ALTER TABLE `employer_employerdaymode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employer_employerholiday`
--

DROP TABLE IF EXISTS `employer_employerholiday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employer_employerholiday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime(6) NOT NULL,
  `employer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employer_employerholiday_employer_id_47d92a0b_fk_auth_user_id` (`employer_id`),
  CONSTRAINT `employer_employerholiday_employer_id_47d92a0b_fk_auth_user_id` FOREIGN KEY (`employer_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employer_employerholiday`
--

LOCK TABLES `employer_employerholiday` WRITE;
/*!40000 ALTER TABLE `employer_employerholiday` DISABLE KEYS */;
/*!40000 ALTER TABLE `employer_employerholiday` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_profile`
--

DROP TABLE IF EXISTS `user_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_employer` tinyint(1) NOT NULL,
  `is_client` tinyint(1) NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `visibility` tinyint(1) NOT NULL,
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `information` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `is_company` tinyint(1) NOT NULL,
  `is_remotely` tinyint(1) NOT NULL,
  `profession` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `user_profile_company_id_0f6aee76_fk_company_company_id` (`company_id`),
  CONSTRAINT `user_profile_company_id_0f6aee76_fk_company_company_id` FOREIGN KEY (`company_id`) REFERENCES `company_company` (`id`),
  CONSTRAINT `user_profile_user_id_8fdce8e2_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_profile`
--

LOCK TABLES `user_profile` WRITE;
/*!40000 ALTER TABLE `user_profile` DISABLE KEYS */;
INSERT INTO `user_profile` VALUES (24,'Azamat Zyntemirov','zyntemirov@gmail.com',0,0,'+996705050035',1,'user/profile_photo.png','Kyrgyzstan','Osh',1,'Hello world',55,5,0,0,'it'),(25,'ФИО',NULL,0,0,NULL,1,'user/profile_photo.png',NULL,NULL,1,NULL,56,NULL,0,0,NULL),(26,'Aman','aman@gmail.com',0,0,'+996778651596',1,'user/zensoft_lrdr8J2.png','Kyrgyzstan','Osh',1,'',57,NULL,0,0,NULL),(27,'Koldoshbek Tashiev','koldoshbektashiev@gmail.com',0,0,'+996771872351',1,'user/profile_photo.png','Kyrgyzstan','Osh',1,'Hello world',58,7,1,0,NULL),(28,'Askar Oracle','oracle-d@gmail.com',0,0,NULL,1,'user/profile_photo.png','Kyrgyzstan','Bishkek',1,'Hello world',59,6,1,0,NULL),(29,'Keldibay Baatyrov','keshabot@gmail.com',0,0,'+996775652351',1,'user/profile_photo.png','Kyrgyzstan','Naryn',1,'Hello world',60,5,1,0,NULL),(30,'Maksat Bolushov','makas@gmail.com',1,0,'+996778456532',1,'user/profile_photo.png',NULL,NULL,1,'Hello',61,5,0,0,NULL),(31,'ФИО',NULL,0,0,NULL,1,'user/profile_photo.png',NULL,NULL,1,NULL,62,NULL,0,0,NULL),(32,'Beknur Baltabaev','balta@gmail.com',1,0,'+996771311565',1,'user/profile_photo.png',NULL,NULL,1,'Hello world',64,5,0,0,NULL),(33,'Islambek Turanov','islambek@gmail.com',1,0,'+996771895478',1,'user/profile_photo.png',NULL,NULL,1,'Hellow ow',65,5,0,0,NULL),(34,'Alex Red','alexr@gmail.com',1,0,'+996773717754',1,'user/profile_photo.png',NULL,NULL,1,'Hello world',66,7,0,0,NULL),(35,'Nemo Piece','nemo@gmail.com',1,0,'+996771548965',1,'user/profile_photo.png',NULL,NULL,1,'hello',67,7,0,0,NULL),(36,'Kateline Hook','kate@gmail.com',1,0,'+996773547139',1,'user/profile_photo.png',NULL,NULL,1,'hello',68,7,0,0,NULL),(37,'Aman Kalmanbetov','aman@gmail.com',1,0,'+996701626702',1,'user/profile_photo.png',NULL,NULL,1,'Hello',69,6,0,0,NULL),(38,'Aman','aman@gmail.com',1,0,'+996776459719',1,'user/profile_photo.png',NULL,NULL,1,'a',70,5,0,0,NULL);
/*!40000 ALTER TABLE `user_profile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-30 23:43:01
