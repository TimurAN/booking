const tabLinks = document.querySelectorAll(".tabs a");
const tabPanels = document.querySelectorAll(".tabs-panel");

for (let el of tabLinks) {
    el.addEventListener("click", e => {
        e.preventDefault();

        document.querySelector(".tabs li.active").classList.remove("active");
        document.querySelector(".tabs-panel.active").classList.remove("active");

        const parentListItem = el.parentElement;
        parentListItem.classList.add("active");
        const index = [...parentListItem.parentElement.children].indexOf(parentListItem);

        const panel = [...tabPanels].filter(el => el.getAttribute("data-index") == index);
        panel[0].classList.add("active");
    });
}
;
jQuery(document).ready(function () {
    jQuery('.select').niceSelect();
    jQuery('.add-timebreak-btn').click(function () {
        jQuery(this).siblings('.add-timebreak-form').addClass('active');
    });
    jQuery('.add-timebreak-form .cancel-btn').click(function () {
        jQuery(this).parents('.add-timebreak-form').removeClass('active');
    });
    jQuery('.add-timebreak-form .save-btn').click(function () {
        jQuery(this).parents('.add-timebreak-form').removeClass('active');
        jQuery(this).parents('.add-timebreak-form').siblings('.timebreak-list').append('<div class="timebreak-list__item"><span>12:00</span><span>-</span><span>13:00</span><div class="remove-btn">x</div></div>')
    });

});
jQuery('.add-emp, .edit,.calendar__tusk').click(function () {
    jQuery('.modal').addClass('active');
});
jQuery('.modal-close').click(function () {
    jQuery('.modal').removeClass('active');
});
jQuery('.burger').click(function () {
    jQuery('.aside').addClass('active');
});
jQuery('.burger-x').click(function () {
    jQuery('.aside').removeClass('active');
});
jQuery('.aside-list>li>a').click(function () {
    jQuery(this).next('.aside-list__drop').addClass('active');
});

jQuery('#label-1').click(function () {
    if (jQuery('#checkbox-1').is(':checked')) {
        jQuery('#check-block-1 input:checkbox').prop('checked', false);
    } else {
        jQuery('#check-block-1 input:checkbox').prop('checked', true);
    }
    ;
});
jQuery('#label-2').click(function () {
    if (jQuery('#checkbox-2').is(':checked')) {
        jQuery('#check-block-2 input:checkbox').prop('checked', false);
    } else {
        jQuery('#check-block-2 input:checkbox').prop('checked', true);
    }
    ;
});
jQuery('#label-3').click(function () {
    if (jQuery('#checkbox-3').is(':checked')) {
        jQuery('#check-block-3 input:checkbox').prop('checked', false);
    } else {
        jQuery('#check-block-3 input:checkbox').prop('checked', true);
    }
    ;
});
jQuery('#label-4').click(function () {
    if (jQuery('#checkbox-4').is(':checked')) {
        jQuery('#check-block-4 input:checkbox').prop('checked', false);
    } else {
        jQuery('#check-block-4 input:checkbox').prop('checked', true);
    }
    ;
});

// jQuery('#big-calendar__months').datepicker({
//     firstDay: 0,
//     weekends: [0],
//     inline: true,
//     showOtherMonths: false,
//     multipleDates: true,
//     view: "days",
//     navTitles: {
//         days: 'MM <i>yyyy</i>',
//         months: 'yyyy',
//         years: 'yyyy'
//     }
// });

let counter = 0;
if (jQuery('div').hasClass('big-calendar')) {
    jQuery('#calendar').calendar({ //https://www.bootstrap-year-calendar.com/
        language: "ru",
        clickDay: function (e) {

            var day = e.date.getDate();
            var month = parseInt(e.date.getMonth()) + 1;
            var year = e.date.getFullYear();
            var date = year + '-' + month + '-' +  day;

            let holiday = document.getElementById("holiday_days");
            if (holiday.innerHTML.includes(date)){
                let input = document.querySelector('input[value="'+date+'"]');
                holiday.removeChild(input);
			}else {
            	holiday.insertAdjacentHTML('beforeend',
                    '<input id="day'+counter+'" type="text" name="holiday'+counter+'" value="'+date+'">');
			}
            jQuery(e.element).children('.day-content').toggleClass('active');
            counter += 1;
        }
    });
}
if (jQuery('div').hasClass('employee-list')) {
    jQuery(function () {
        jQuery("#sortable").sortable({
            revert: true,
            handle: ".drag"
        });
        jQuery(".meet-t__ava").disableSelection();
    });
}
;