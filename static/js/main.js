$('.com-card__info').click(function () {
    $('.modal').addClass('active');
});
$('.modal-close').click(function () {
    $('.modal').removeClass('active');
});
$('.burger').click(function () {
    $('.aside').addClass('active');
});
$('.burger-x').click(function () {
    $('.aside').removeClass('active');
});
if ($('*').hasClass("calendar")) {
    $('#calendar').datepicker({

        firstDay: 0,
        weekends: [0],
        inline: true,
        showOtherMonths: false,
        onSelect: function (fd, d, picker) {
            // Ничего не делаем если выделение было снято

            var day = d.getDate();
            var month = parseInt(d.getMonth()) + 1;
            var year = d.getFullYear();
            var date = year + '-' + month + '-' + day;
            document.getElementById('hidden_calendar').value = date;


            let emp_id = $('input[name=specialist]:checked').val();
            // document.getElementById('checked_employer').outerHTML = document.getElementById(emp_id).outerHTML;
            mainData.employer = emp_id;
            mainData.date = date;
            $.ajax({
                url: 'http://' + hostIp + '/en/booking/ajax/date/' + emp_id + '/' + date,
                type: 'get',
                dataType: 'html',
                success: function (data) {
                    var temp = document.createElement('div');
                    temp.innerHTML = data;
                    document.getElementById("choose_period").replaceWith(temp)
                },
                error: function (xhr, status) {
                    alert("Sorry, there was a problem!");
                },

            });


        }

    });
}
jQuery('.sign_in_btn').click(function () {
    jQuery('.popup').removeClass('active');
    jQuery('.sign-in,.black-overlay').addClass('active');
});
jQuery('.sign_up_btn').click(function () {
    jQuery('.popup').removeClass('active');
    jQuery('.sign-up,.black-overlay').addClass('active');
});
jQuery('.reset_pass_btn').click(function () {
    jQuery('.popup').removeClass('active');
    jQuery('.reset-pass,.black-overlay').addClass('active');
});
jQuery('.black-overlay').click(function () {
    jQuery('.popup').removeClass('active');
    jQuery(this).removeClass('active');
});
$('.set-time h2').click(function () {
    $(this).toggleClass('active');
});
$('.set-time h2').click(function () {
    $(this).next('.js-set-time').slideToggle();
});
if ($('*').hasClass("select")) {
    $('.select').niceSelect();
}
$('.user-modal__cap.js').click(function () {
    $(this).toggleClass('active');
});
$('.user-modal__cap.js').click(function () {
    $(this).next('.js-user').slideToggle();
});