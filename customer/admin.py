from django.contrib import admin
from modeltranslation.admin import TranslationAdmin
from .models import Favorite, Booking, Period


class ViewFavorite(admin.ModelAdmin):
    list_display = ["customer", "company", "add_date"]
    search_fields = ["company"]


class ViewBookingAdmin(admin.ModelAdmin):
    list_display = [ "employer"]
    search_fields = []


class ViewPerid(admin.ModelAdmin):
    list_display = ["booking", "start_period", "end_period"]
    search_fields = ["booking"]


class DayTranslatedEntryAdmin(ViewBookingAdmin, TranslationAdmin):
    pass


admin.site.register(Favorite, ViewFavorite),
admin.site.register(Booking, ViewBookingAdmin),
admin.site.register(Period, ViewPerid),
# admin.site.register(Booking, DayTranslatedEntryAdmin)
