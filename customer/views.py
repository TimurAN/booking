from django.core.paginator import Paginator
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from django.db.models import Q

from booking.decorators import client_required
from company.models import *
from customer.models import *
from django.contrib import messages
from customer.forms import CustomerProfileForm
from user.models import Profile
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from .models import Booking
from datetime import date as Date, time
from employer.diary import *
from employer.models import *

from django.core.mail import EmailMessage
from django.core.mail import send_mail
from django.conf import settings

import random
import hashlib


# @method_decorator((login_required, client_required), name='dispatch')
class CategoryView(TemplateView):
    template_name = 'customer/list_company.html'
    companies_per_page = 3

    def get(self, request, *args, **kwargs):
        title = kwargs['slug']
        if title == 'all':
            companies = Company.objects.all()
        else:
            companies = Company.objects.filter(category__title=title)
        search_query = request.GET.get('search_company', '')
        page = request.GET.get('page', 1)

        if search_query:
            companies = companies.filter(Q(company_name__icontains=search_query))

        paginator = Paginator(companies, self.companies_per_page)
        result = paginator.get_page(page)

        return render(request, self.template_name, context={
            'companies': result,
            'path': title,
            'count': companies.count(),
        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    # @method_decorator(login_required)
    # def dispatch(self, *args, **kwargs):
    #     return super(CategoryView, self).dispatch(*args, **kwargs)


# @method_decorator((login_required, client_required), name='dispatch')
class AddBookingView(TemplateView):
    template_name = 'customer/add_booking.html'

    def get(self, request, *args, **kwargs):
        name = kwargs['name']
        company = Company.objects.get(company_name=name)
        ser_sub = []
        for i in company.employer.all():
            for j in i.user.services.all():
                if j.service_sub_category not in ser_sub:
                    ser_sub.append(j.service_sub_category)

        sp = []
        time = []

        return render(request, self.template_name, context={
            'company': company,
            'service_subs': ser_sub,
            'specialists': sp,
            'times': time,
        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    # @method_decorator(login_required)
    # def dispatch(self, *args, **kwargs):
    #     return super(AddBookingView, self).dispatch(*args, **kwargs)


def submitBooking(request):
    d = dict(request.POST)

    try:
        ser = request.POST['services']
        spec = request.POST['specialist']
        dateP = request.POST['date']

        contact_name = request.POST['contact_name']
        contact_phone = request.POST['contact_phone']
        contact_email = request.POST['contact_email']
        contact_comments = request.POST['contact_comments']

    except:
        return HttpResponse('Talaalar toluk emes')
    try:

        dateP = dateP.split("-")
        specialist = User.objects.get(pk=int(spec))
        date = Date(int(dateP[0]), int(dateP[1]), int(dateP[2]))
        if request.user.is_authenticated:
            booking = Booking.objects.create(
                employer=specialist,
                customer=request.user,
                company=specialist.profile.company,
                booking_date=date,
                contact_name=contact_name,
                contact_phone=contact_phone,
                contact_email=contact_email,
                contact_comments=contact_comments,
                status='В ожидании',

            )
        else:
            booking = Booking.objects.create(
                employer=specialist,
                company=specialist.profile.company,
                booking_date=date,
                contact_name=contact_name,
                contact_phone=contact_phone,
                contact_email=contact_email,
                contact_comments=contact_comments,
                status='В ожидании',

            )

    except:
        return HttpResponse('<h2>Booking kata</h2>')

    for i in d['times']:
        i = i.replace(' ', '')
        start, end = i.split('-')

        start = start.split(':')
        end = end.split(':')

        st = time(int(start[0]), int(start[1]))
        ed = time(int(end[0]), int(end[1]))

        Period.objects.create(booking=booking, start_period=st, end_period=ed)
        messages.info(request, 'Booking confirmed!')

    for i in d['services']:
        service = ServiceSubCategory.objects.get(pk=int(i))
        BookingService.objects.create(booking=booking, service=service)

    return redirect('index')


@method_decorator((login_required, client_required), name='dispatch')
class AboutView(TemplateView):
    template_name = 'customer/index-8.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context={

        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AboutView, self).dispatch(*args, **kwargs)


# @method_decorator((login_required, client_required), name='dispatch')
class AboutCompanyView(TemplateView):
    template_name = 'customer/about_company.html'

    def get(self, request, *args, **kwargs):
        company = Company.objects.get(id=kwargs['company_id'])
        return render(request, self.template_name, context={
            'company': company,
        })

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
        })

    # @method_decorator(login_required)
    # def dispatch(self, *args, **kwargs):
    #     return super(AboutCompanyView, self).dispatch(*args, **kwargs)


'''
   /////////////// Aman Men Sherkazy jasap bawtadym
'''


@method_decorator((login_required, client_required), name='dispatch')
class CustomerProfile(TemplateView):
    template_name = 'customer/settings.html'

    def get(self, request, *args, **kwargs):
        try:
            form = CustomerProfileForm(instance=request.user.profile)
        except(Exception):
            Profile.objects.create(user=request.user)
            form = CustomerProfileForm(instance=request.user.profile)

        return render(request, self.template_name, context={
            'form': form,
        })

    def post(self, request, *args, **kwargs):
        form = CustomerProfileForm(request.POST, request.FILES, instance=request.user.profile)
        if form.is_valid():
            form.save()
        return render(request, self.template_name, context={
            'form': form,
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CustomerProfile, self).dispatch(*args, **kwargs)


@method_decorator((login_required, client_required), name='dispatch')
class MyBookingView(TemplateView):
    template_name = 'customer/my_booking.html'

    def get(self, request, *args, **kwargs):
        bookings = Booking.objects.filter(customer=request.user)
        return render(request, self.template_name, context={
            'bookings': bookings,
        })

    def post(self, request, *args, **kwargs):
        form = CustomerProfileForm(request.POST, request.FILES, instance=request.user.profile)
        if form.is_valid():
            form.save()
        return render(request, self.template_name, context={
            'form': form,
        })

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MyBookingView, self).dispatch(*args, **kwargs)


@csrf_exempt
def ajaxEmployerView(request, compId):
    template_name = 'customer/ajaxEmloyer.html'
    d = dict(request.POST)

    service_ids = [int(i) for i in d['services[]']]
    employer = []
    emps = User.objects.filter(profile__company_id=compId)

    for emp in emps:
        servicesSubCat = ServiceSubCategory.objects.filter(sub_category__employer_id=emp.id)
        emp_ser = [i.id for i in servicesSubCat]
        flag = True
        for i in service_ids:
            if i not in emp_ser:
                flag = False
        if flag:
            employer.append(emp)

    return render(request, template_name, context={
        'employers': employer
    })


def ajaxDateView(request, emp_id, date):
    template_name = 'customer/ajaxDate.html'
    employer = User.objects.get(pk=emp_id)
    date = [int(i) for i in date.split('-')]
    dt = Date(date[0], date[1], date[2])
    try:
        day = EmployerDayMode.objects.get(employer=employer, day=dt.isoweekday() - 1)
        times = out_period(get_all_time(day, dt))
    except:
        times = []

    holiday = EmployerHoliday.objects.filter(employer=employer, date=dt)

    if holiday:
        times = []
    return render(request, template_name, context={
        'times': times
    })


def ajaxSendView(request, email):
    code = generate_nonce()

    email_address = [email]
    email = EmailMessage()
    email.subject = 'Потвердите номер заказа'

    email.from_email = "AWWWARDS <alemstroi.ru@gmail.com>"
    email.to = email_address
    email.attach(content='Ваш код:{}'.format(code), mimetype="text/html")

    email.send()

    # subject = 'AWWWARDS <alemstroi.ru@gmail.com>'
    # message = 'Ваш код:{}'.format(code)
    # email_from = settings.EMAIL_HOST_USER
    # recipient_list = [email_address, ]
    # send_mail(subject, message, email_from, recipient_list)

    # hash code

    hash_code = hashlib.md5(code.encode())
    hash = hash_code.hexdigest()

    return JsonResponse({'code': hash})


def generate_nonce(length=5):
    """Generate pseudorandom number."""
    return ''.join([str(random.randint(0, 9)) for i in range(length)])


@csrf_exempt
def ajaxConf(request):
    template_name = 'customer/ajaxConf.html'
    d = dict(request.POST)
    serviceIds = [int(i) for i in d['services[]']]
    empId = [int(i) for i in d['employer']][0]
    dateVal = [i for i in d['date']][0]
    periodVal = [i for i in d['period[]']]

    emp = User.objects.get(id=empId)
    serSub = []
    for i in serviceIds:
        serSub.append(Service.objects.get(employer_id=empId, service_sub_category_id=i))

    totaltime = len(serSub) * 30
    totalmoney = sum([i.price for i in serSub])
    # print(empId)
    # print(dateVal)
    # print(periodVal)
    # print(serSub[0])

    return render(request, template_name, context={
        'employer': emp,
        'services': serSub,
        'dateValue': dateVal,
        'periodValue': periodVal,
        'totaltime': totaltime,
        'totalmoney': totalmoney

    })
