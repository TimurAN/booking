from django import forms
from django.contrib.auth.models import User
#
#
# class UserRegistrationForm(forms.ModelForm):
#     # suppliers_name = forms.ModelChoiceField(queryset=Supplier.objects.all(),
#     #                                         widget=forms.Select(attrs={'class': 'my-select'}))
#
#     name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'input-text'}))
#     quantity = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'input-text'}))
#     barcode = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'input-text'}))
#     image = forms.ImageField(required=False)
#
#     class Meta:
#         model = User
#         fields = (
#             'username', 'first_name', 'last_name')
from user.models import Profile
from .models import Booking


class CustomerProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['name', 'email', 'phone', 'image']


# class MyBookingForm(forms.ModelForm):
#     class Meta:
#         model = Booking
#         fields = []
