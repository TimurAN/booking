"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from customer.views import *

urlpatterns = [
    path('company/<str:slug>/', CategoryView.as_view(), name='choose_company'),
    path('add/<str:name>/', AddBookingView.as_view(), name='add_booking'),
    path('submit/', submitBooking, name='submit_booking'),

    path('about/', AboutView.as_view(), name='about'),
    path('about-company/<int:company_id>', AboutCompanyView.as_view(), name='about_company'),

    # ''' Aman jasap jatat ''',

    path('settings/', CustomerProfile.as_view(), name='customer_settings'),
    path('booking/', MyBookingView.as_view(), name='my_booking'),
    path('ajax/employer/<str:compId>', ajaxEmployerView, name='ajax_get_employer'),
    path('ajax/date/<str:emp_id>/<str:date>', ajaxDateView, name='ajax_get_date'),
    path('ajax/sendEmail/<str:email>', ajaxSendView, name='ajax_send_email'),
    path('ajax/confirm', ajaxConf, name='ajax_conf'),


]
