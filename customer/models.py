from django.contrib.auth.models import User
from django.db import models
from user.models import Profile
from company.models import *
from django.utils import timezone


class Favorite(models.Model):
    customer = models.ForeignKey(Profile, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    add_date = models.DateTimeField(default=timezone.now, editable=False)


class Booking(models.Model):
    # WEEK = (('waiting', "В ожидании"), ('accepted', "Выполнено"), ('rejected_client', "Отменено клиентом"), ('rejected_employer', "Отменено сотрудником"))
    WEEK = (
        ('В ожидании', "В ожидании"),
        ('Выполнено', "Выполнено"),
        ('Отменено клиентом', "Отменено клиентом"),
        ('Отменено сотрудником', "Отменено сотрудником"),
    )

    customer = models.ForeignKey(User, related_name='customer_booking', on_delete=models.CASCADE, null=True, blank=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    service = models.ForeignKey(Service, on_delete=models.CASCADE, null=True)  # null
    employer = models.ForeignKey(User, related_name='employer_booking', on_delete=models.CASCADE)
    # part_day = models.CharField(max_length=30, choices=['morning', 'afternoon', 'evening'])

    create_date = models.DateTimeField(default=timezone.now, editable=False)
    booking_date = models.DateField(default=timezone.now, editable=False)
    status = models.CharField(max_length=32, blank=True, null=True, choices=WEEK)

    contact_name = models.CharField(max_length=64, blank=True, null=True)
    contact_phone = models.CharField(max_length=64, blank=True, null=True)
    contact_email = models.CharField(max_length=64, blank=True, null=True)
    contact_comments = models.CharField(max_length=64, blank=True, null=True)

    def __str__(self):
        # return f'{self.customer.profile}_{self.status}'
        return str(self.customer)


# class BookingDate(models.Model):
#     booking = models.ForeignKey(Booking, on_delete=models.CASCADE)


class Period(models.Model):
    booking = models.ForeignKey(Booking, on_delete=models.CASCADE, related_name="booking_periods")
    start_period = models.TimeField()
    end_period = models.TimeField()

    # part_of_day = models.CharField(max_length=30, default="day", choices=p_day)


class BookingService(models.Model):
    booking = models.ForeignKey(Booking, on_delete=models.CASCADE, related_name="booking_services")
    service = models.ForeignKey(ServiceSubCategory, on_delete=models.CASCADE)
# УШУНДАЙ КЫЛСАК КАНДАЙ БОЛОТ?
# sldkfjkdlsjf

# morning = models.BooleanField()
# day = models.BooleanField()
# evening = models.BooleanField()

# class PartDayTime:
#     booking = models.ForeignKey(Booking, on_delete=models.CASCADE)

# class Week(models.Model):
#     days = models.CharField(max_length=30,
#                             choices=['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'])
#     morning = models.BooleanField()
#     day = models.BooleanField()
#     evening = models.BooleanField()
