from modeltranslation.translator import register, TranslationOptions
from .models import Booking


@register(Booking)
class DayTranslationOptions(TranslationOptions):
    fields = ()
