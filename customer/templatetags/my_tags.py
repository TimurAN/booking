from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def param_replace(context, **kwargs):
    d = context['request'].GET.copy()
    for k, v in kwargs.items():
        d[k] = v
    for k in [k for k, v in d.items() if not v]:
        del d[k]
    return d.urlencode()


@register.simple_tag
def active(request, pattern):
    path = request.path
    if path == pattern:
        return 'active'
    return ''


@register.simple_tag
def active_setting_inf(request):
    path = request.path
    if '/company/settings/information/' in path:
        return 'active'
    return ''


@register.simple_tag
def active_setting(request):
    path = request.path
    if '/company/settings/' in path:
        return 'active'
    return ''


@register.simple_tag
def active_calendar(request, pattern):
    path = request.path
    id = path.rsplit('/', 2)[1]
    if id == str(pattern):
        return 'active'
    return ''


@register.simple_tag
def display_is_break(is_break):
    if is_break:
        return 'inline-flex'
    else:
        return 'none'
